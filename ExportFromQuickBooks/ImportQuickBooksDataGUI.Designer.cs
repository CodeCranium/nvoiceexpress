﻿namespace ExportFromQuickBooks
{
    partial class form_ImportDataQuickBooks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_PathToExcelQuickbooksData = new System.Windows.Forms.Label();
            this.txtbx_PathToXLS = new System.Windows.Forms.TextBox();
            this.txtbx_DbInstance = new System.Windows.Forms.TextBox();
            this.lbl_nVoiceDatabaseInstance = new System.Windows.Forms.Label();
            this.txtbx_DbName = new System.Windows.Forms.TextBox();
            this.lbl_nVoiceDatabaseName = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_PathToExcelQuickbooksData
            // 
            this.lbl_PathToExcelQuickbooksData.AutoSize = true;
            this.lbl_PathToExcelQuickbooksData.Location = new System.Drawing.Point(12, 15);
            this.lbl_PathToExcelQuickbooksData.Name = "lbl_PathToExcelQuickbooksData";
            this.lbl_PathToExcelQuickbooksData.Size = new System.Drawing.Size(198, 13);
            this.lbl_PathToExcelQuickbooksData.TabIndex = 0;
            this.lbl_PathToExcelQuickbooksData.Text = "Path To QuickBooks Exported XLS data";
            // 
            // txtbx_PathToXLS
            // 
            this.txtbx_PathToXLS.Location = new System.Drawing.Point(15, 31);
            this.txtbx_PathToXLS.Name = "txtbx_PathToXLS";
            this.txtbx_PathToXLS.Size = new System.Drawing.Size(370, 20);
            this.txtbx_PathToXLS.TabIndex = 1;
            // 
            // txtbx_DbInstance
            // 
            this.txtbx_DbInstance.Location = new System.Drawing.Point(151, 84);
            this.txtbx_DbInstance.Name = "txtbx_DbInstance";
            this.txtbx_DbInstance.ReadOnly = true;
            this.txtbx_DbInstance.Size = new System.Drawing.Size(234, 20);
            this.txtbx_DbInstance.TabIndex = 3;
            // 
            // lbl_nVoiceDatabaseInstance
            // 
            this.lbl_nVoiceDatabaseInstance.AutoSize = true;
            this.lbl_nVoiceDatabaseInstance.Location = new System.Drawing.Point(12, 87);
            this.lbl_nVoiceDatabaseInstance.Name = "lbl_nVoiceDatabaseInstance";
            this.lbl_nVoiceDatabaseInstance.Size = new System.Drawing.Size(133, 13);
            this.lbl_nVoiceDatabaseInstance.TabIndex = 2;
            this.lbl_nVoiceDatabaseInstance.Text = "nVoice Database Instance";
            // 
            // txtbx_DbName
            // 
            this.txtbx_DbName.Location = new System.Drawing.Point(151, 106);
            this.txtbx_DbName.Name = "txtbx_DbName";
            this.txtbx_DbName.ReadOnly = true;
            this.txtbx_DbName.Size = new System.Drawing.Size(234, 20);
            this.txtbx_DbName.TabIndex = 5;
            // 
            // lbl_nVoiceDatabaseName
            // 
            this.lbl_nVoiceDatabaseName.AutoSize = true;
            this.lbl_nVoiceDatabaseName.Location = new System.Drawing.Point(12, 109);
            this.lbl_nVoiceDatabaseName.Name = "lbl_nVoiceDatabaseName";
            this.lbl_nVoiceDatabaseName.Size = new System.Drawing.Size(120, 13);
            this.lbl_nVoiceDatabaseName.TabIndex = 4;
            this.lbl_nVoiceDatabaseName.Text = "nVoice Database Name";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(258, 149);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(127, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Import Customer Data";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.ImportXlsData);
            // 
            // form_ImportDataQuickBooks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(397, 185);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtbx_DbName);
            this.Controls.Add(this.lbl_nVoiceDatabaseName);
            this.Controls.Add(this.txtbx_DbInstance);
            this.Controls.Add(this.lbl_nVoiceDatabaseInstance);
            this.Controls.Add(this.txtbx_PathToXLS);
            this.Controls.Add(this.lbl_PathToExcelQuickbooksData);
            this.Name = "form_ImportDataQuickBooks";
            this.Text = "Import Data from QuickBooks";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_PathToExcelQuickbooksData;
        private System.Windows.Forms.TextBox txtbx_PathToXLS;
        private System.Windows.Forms.TextBox txtbx_DbInstance;
        private System.Windows.Forms.Label lbl_nVoiceDatabaseInstance;
        private System.Windows.Forms.TextBox txtbx_DbName;
        private System.Windows.Forms.Label lbl_nVoiceDatabaseName;
        private System.Windows.Forms.Button button1;
    }
}

