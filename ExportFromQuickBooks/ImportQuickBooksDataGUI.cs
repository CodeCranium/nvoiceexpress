﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using nVoice_Express;

namespace ExportFromQuickBooks
{
    public partial class form_ImportDataQuickBooks : Form
    {
        nVoice_Express.nVoiceEntities nVoiceDB = new nVoice_Express.nVoiceEntities();

        public form_ImportDataQuickBooks()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GetApplicationDatabaseDetails();
        }

        private void GetApplicationDatabaseDetails()
        {
            txtbx_DbInstance.Text = nVoiceDB.Database.Connection.DataSource;
            txtbx_DbName.Text = nVoiceDB.Database.Connection.Database;
        }

        private void ImportXlsData(object sender, EventArgs e)
        {

            try
            {
                string xlsDocPath = txtbx_PathToXLS.Text;
                if (! String.IsNullOrEmpty(xlsDocPath))
                {
                
                    ExcelLinqData quickbooksData = new ExcelLinqData();
                    var importData = quickbooksData.GetExcelData(xlsDocPath, "Sheet1");

                    // The returned data for [importData] will be in the QuickBooksModel type - Process the data and input it into the nVoice DB
                    foreach (var quickBooksModel in importData)
                    {
                        nVoiceDB.Clients.Add(new Client()
                                                 {
                                                     Firstname = quickBooksModel.FirstName,
                                                     Lastname = quickBooksModel.LastName,
                                                     Address = GetAddress.Address(quickBooksModel.BillingAddress),
                                                     City = GetAddress.City,
                                                     State = GetAddress.State,
                                                     Zipcode = GetAddress.ZipCode,
                                                     EmailAddress = quickBooksModel.Email,
                                                     PhoneNumber = quickBooksModel.PhoneNumber,
                                                 });
                    }

                    nVoiceDB.SaveChanges();

                    new ResultMessage() { ResultMessageInformation = String.Format("Data has been saved and updated to DB Source: {0}, Database: ", nVoiceDB.Database.Connection.DataSource, nVoiceDB.Database.Connection.Database), ResultType = ResultMessage.resultType.Info }.Alert(); 
                }
            }
            catch (Exception ex)
            {
                new ResultMessage() { ResultMessageInformation = ex.Message, ResultType = ResultMessage.resultType.Error }.Alert(); 
            }
            
        }
    }

    internal class GetAddress
    {
        public static string City { get; set; }
        public static string State { get; set; }
        public static string ZipCode { get; set; }
        public static string Address(string billingAddress)
        {
            if (billingAddress != null)
            {
                var ba = billingAddress.Split(',');

                if (ba.Length > 1) City = ba[1];
                if (ba[2].Trim().Split(' ').Length > 0) State = ba[2].Trim().Split(' ')[0];
                if (ba[2].Trim().Split(' ').Length > 1) ZipCode = ba[2].Trim().Split(' ')[1];

                return ba[0];
            }
            return null;
        }
    }
}
