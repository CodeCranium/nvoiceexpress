﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using nVoice_Express;

namespace ExportFromQuickBooks
{
    class ExcelLinqData
    {

        private void QuickBooksFields()
        {
            // http://support.quickbooks.intuit.com/support/pages/inproducthelp/core/qb2k12/contentpackage/thirdparty/excel/info_customer_required_fields.html?family=pro
            
            //JOB OR CUSTOMER NAME  (REQUIRED)
            //OPENING BALANCE
            //OPENING BALANCE AS OF
            //COMPANY NAME
            //SALUTATION
            //FIRST NAME
            //MIDDLE INITIAL
            //LAST NAME
            //CONTACT
            //PHONE
            //FAX
            //ALTERNATE PHONE
            //ALTERNATE CONTACT
            //EMAIL
            //BILLING ADDRESS 1 through BILLING ADDRESS 5
            //SHIPPING ADDRESS 1 through SHIPPING ADDRESS 5
            //CUSTOMER TYPE
            //TERMS
            //SALES REP
            //PREFERRED SEND METHOD
            //TAX CODE
            //TAX ITEM
            //RESALE NUMBER
            //PRICE LEVEL
            //ACCOUNT NUMBER
            //CREDIT LIMIT
            //PREFERRED PAYMENT METHOD
            //CREDIT CARD NUMBER
            //NAME ON CARD
            //CREDIT CARD ADDRESS
            //CREDIT CARD ZIP CODE
            //JOB STATUS
            //JOB START DATE
            //JOB PROJECTED END
            //JOB END DATE
            //JOB DESCRIPTION
            //JOB TYPE
            //IS INACTIVE
            //NOTE
        }

        /// <summary>
        /// Usage Example: 
        /// 
        ///     var x1 = GetExcelData(@"C:\Temp\test.xlsx", "Sheet1", "Name");
        ///     foreach (var item in x1)
        ///     {
        ///         Console.WriteLine(item);
        ///     }
        /// </summary>
        public List<QuickBooksModel> GetExcelData(string excelFileName, string excelSheetName)
        {
            try
            {
                var book = new LinqToExcel.ExcelQueryFactory(excelFileName);

                List<QuickBooksModel> returnList = new List<QuickBooksModel>();

                var query = from row in book.Worksheet(excelSheetName)
                            let x = new QuickBooksModel()
                                          {
                                              FirstName = row["FIRST NAME"].Cast<string>(),
                                              LastName = row["LAST NAME"].Cast<string>(),
                                              PhoneNumber = row["PHONE"].Cast<string>(),
                                              Email = row["EMAIL"].Cast<string>(),
                                              BillingAddress = row["BILLING ADDRESS 1"].Cast<string>(),
                                          }
                            select x;

                foreach (var quickBooksModel in query)
                {
                    returnList.Add(quickBooksModel);
                }
                return returnList;
            }
            catch (InvalidOperationException ex)
            {
               new ResultMessage() { ResultMessageInformation = ex.Message, ResultType = ResultMessage.resultType.Error }.Alert();         
               return null;
            }

        }

    }
}
