﻿namespace nVoice_Express
{
    partial class AddCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_DeleteCustomer = new System.Windows.Forms.Button();
            this.txtbx_PhoneNumber = new System.Windows.Forms.TextBox();
            this.lbl_PhoneNumber = new System.Windows.Forms.Label();
            this.txtbx_EmailAddress = new System.Windows.Forms.TextBox();
            this.lbl_EmailAddress = new System.Windows.Forms.Label();
            this.txtbx_ZipCode = new System.Windows.Forms.TextBox();
            this.lbl_Zipcode = new System.Windows.Forms.Label();
            this.txtbx_State = new System.Windows.Forms.TextBox();
            this.lbl_State = new System.Windows.Forms.Label();
            this.txtbx_City = new System.Windows.Forms.TextBox();
            this.lbl_City = new System.Windows.Forms.Label();
            this.txtbx_StreetAddress = new System.Windows.Forms.TextBox();
            this.lbl_AddressStreet = new System.Windows.Forms.Label();
            this.txtbx_LastName = new System.Windows.Forms.TextBox();
            this.lbl_Lastname = new System.Windows.Forms.Label();
            this.txtbx_FirstName = new System.Windows.Forms.TextBox();
            this.lbl_FirstName = new System.Windows.Forms.Label();
            this.btn_AddCustomer = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_DeleteCustomer);
            this.panel1.Controls.Add(this.txtbx_PhoneNumber);
            this.panel1.Controls.Add(this.lbl_PhoneNumber);
            this.panel1.Controls.Add(this.txtbx_EmailAddress);
            this.panel1.Controls.Add(this.lbl_EmailAddress);
            this.panel1.Controls.Add(this.txtbx_ZipCode);
            this.panel1.Controls.Add(this.lbl_Zipcode);
            this.panel1.Controls.Add(this.txtbx_State);
            this.panel1.Controls.Add(this.lbl_State);
            this.panel1.Controls.Add(this.txtbx_City);
            this.panel1.Controls.Add(this.lbl_City);
            this.panel1.Controls.Add(this.txtbx_StreetAddress);
            this.panel1.Controls.Add(this.lbl_AddressStreet);
            this.panel1.Controls.Add(this.txtbx_LastName);
            this.panel1.Controls.Add(this.lbl_Lastname);
            this.panel1.Controls.Add(this.txtbx_FirstName);
            this.panel1.Controls.Add(this.lbl_FirstName);
            this.panel1.Controls.Add(this.btn_AddCustomer);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(305, 237);
            this.panel1.TabIndex = 3;
            // 
            // btn_DeleteCustomer
            // 
            this.btn_DeleteCustomer.ForeColor = System.Drawing.Color.DarkRed;
            this.btn_DeleteCustomer.Location = new System.Drawing.Point(82, 203);
            this.btn_DeleteCustomer.Name = "btn_DeleteCustomer";
            this.btn_DeleteCustomer.Size = new System.Drawing.Size(102, 23);
            this.btn_DeleteCustomer.TabIndex = 20;
            this.btn_DeleteCustomer.Text = "&Delete Customer";
            this.btn_DeleteCustomer.UseVisualStyleBackColor = true;
            this.btn_DeleteCustomer.Visible = false;
            this.btn_DeleteCustomer.Click += new System.EventHandler(this.btn_DeleteCustomer_Click);
            // 
            // txtbx_PhoneNumber
            // 
            this.txtbx_PhoneNumber.Location = new System.Drawing.Point(95, 165);
            this.txtbx_PhoneNumber.Name = "txtbx_PhoneNumber";
            this.txtbx_PhoneNumber.Size = new System.Drawing.Size(197, 20);
            this.txtbx_PhoneNumber.TabIndex = 19;
            // 
            // lbl_PhoneNumber
            // 
            this.lbl_PhoneNumber.AutoSize = true;
            this.lbl_PhoneNumber.Location = new System.Drawing.Point(12, 168);
            this.lbl_PhoneNumber.Name = "lbl_PhoneNumber";
            this.lbl_PhoneNumber.Size = new System.Drawing.Size(78, 13);
            this.lbl_PhoneNumber.TabIndex = 18;
            this.lbl_PhoneNumber.Text = "Phone Number";
            // 
            // txtbx_EmailAddress
            // 
            this.txtbx_EmailAddress.Location = new System.Drawing.Point(95, 142);
            this.txtbx_EmailAddress.Name = "txtbx_EmailAddress";
            this.txtbx_EmailAddress.Size = new System.Drawing.Size(197, 20);
            this.txtbx_EmailAddress.TabIndex = 17;
            // 
            // lbl_EmailAddress
            // 
            this.lbl_EmailAddress.AutoSize = true;
            this.lbl_EmailAddress.Location = new System.Drawing.Point(12, 145);
            this.lbl_EmailAddress.Name = "lbl_EmailAddress";
            this.lbl_EmailAddress.Size = new System.Drawing.Size(73, 13);
            this.lbl_EmailAddress.TabIndex = 16;
            this.lbl_EmailAddress.Text = "Email Address";
            // 
            // txtbx_ZipCode
            // 
            this.txtbx_ZipCode.Location = new System.Drawing.Point(95, 119);
            this.txtbx_ZipCode.Name = "txtbx_ZipCode";
            this.txtbx_ZipCode.Size = new System.Drawing.Size(197, 20);
            this.txtbx_ZipCode.TabIndex = 15;
            // 
            // lbl_Zipcode
            // 
            this.lbl_Zipcode.AutoSize = true;
            this.lbl_Zipcode.Location = new System.Drawing.Point(12, 122);
            this.lbl_Zipcode.Name = "lbl_Zipcode";
            this.lbl_Zipcode.Size = new System.Drawing.Size(50, 13);
            this.lbl_Zipcode.TabIndex = 14;
            this.lbl_Zipcode.Text = "Zip Code";
            // 
            // txtbx_State
            // 
            this.txtbx_State.Location = new System.Drawing.Point(95, 96);
            this.txtbx_State.Name = "txtbx_State";
            this.txtbx_State.Size = new System.Drawing.Size(197, 20);
            this.txtbx_State.TabIndex = 13;
            // 
            // lbl_State
            // 
            this.lbl_State.AutoSize = true;
            this.lbl_State.Location = new System.Drawing.Point(12, 99);
            this.lbl_State.Name = "lbl_State";
            this.lbl_State.Size = new System.Drawing.Size(32, 13);
            this.lbl_State.TabIndex = 12;
            this.lbl_State.Text = "State";
            // 
            // txtbx_City
            // 
            this.txtbx_City.Location = new System.Drawing.Point(95, 74);
            this.txtbx_City.Name = "txtbx_City";
            this.txtbx_City.Size = new System.Drawing.Size(197, 20);
            this.txtbx_City.TabIndex = 11;
            // 
            // lbl_City
            // 
            this.lbl_City.AutoSize = true;
            this.lbl_City.Location = new System.Drawing.Point(12, 77);
            this.lbl_City.Name = "lbl_City";
            this.lbl_City.Size = new System.Drawing.Size(24, 13);
            this.lbl_City.TabIndex = 10;
            this.lbl_City.Text = "City";
            // 
            // txtbx_StreetAddress
            // 
            this.txtbx_StreetAddress.Location = new System.Drawing.Point(95, 51);
            this.txtbx_StreetAddress.Name = "txtbx_StreetAddress";
            this.txtbx_StreetAddress.Size = new System.Drawing.Size(197, 20);
            this.txtbx_StreetAddress.TabIndex = 9;
            // 
            // lbl_AddressStreet
            // 
            this.lbl_AddressStreet.AutoSize = true;
            this.lbl_AddressStreet.Location = new System.Drawing.Point(12, 54);
            this.lbl_AddressStreet.Name = "lbl_AddressStreet";
            this.lbl_AddressStreet.Size = new System.Drawing.Size(76, 13);
            this.lbl_AddressStreet.TabIndex = 8;
            this.lbl_AddressStreet.Text = "Street Address";
            // 
            // txtbx_LastName
            // 
            this.txtbx_LastName.Location = new System.Drawing.Point(95, 28);
            this.txtbx_LastName.Name = "txtbx_LastName";
            this.txtbx_LastName.Size = new System.Drawing.Size(197, 20);
            this.txtbx_LastName.TabIndex = 7;
            this.txtbx_LastName.TextChanged += new System.EventHandler(this.CheckForExistingCustomer);
            // 
            // lbl_Lastname
            // 
            this.lbl_Lastname.AutoSize = true;
            this.lbl_Lastname.Location = new System.Drawing.Point(12, 31);
            this.lbl_Lastname.Name = "lbl_Lastname";
            this.lbl_Lastname.Size = new System.Drawing.Size(58, 13);
            this.lbl_Lastname.TabIndex = 6;
            this.lbl_Lastname.Text = "Last Name";
            // 
            // txtbx_FirstName
            // 
            this.txtbx_FirstName.Location = new System.Drawing.Point(95, 6);
            this.txtbx_FirstName.Name = "txtbx_FirstName";
            this.txtbx_FirstName.Size = new System.Drawing.Size(197, 20);
            this.txtbx_FirstName.TabIndex = 5;
            this.txtbx_FirstName.TextChanged += new System.EventHandler(this.CheckForExistingCustomer);
            // 
            // lbl_FirstName
            // 
            this.lbl_FirstName.AutoSize = true;
            this.lbl_FirstName.Location = new System.Drawing.Point(12, 9);
            this.lbl_FirstName.Name = "lbl_FirstName";
            this.lbl_FirstName.Size = new System.Drawing.Size(57, 13);
            this.lbl_FirstName.TabIndex = 4;
            this.lbl_FirstName.Text = "First Name";
            // 
            // btn_AddCustomer
            // 
            this.btn_AddCustomer.Location = new System.Drawing.Point(190, 203);
            this.btn_AddCustomer.Name = "btn_AddCustomer";
            this.btn_AddCustomer.Size = new System.Drawing.Size(102, 23);
            this.btn_AddCustomer.TabIndex = 3;
            this.btn_AddCustomer.Text = "&Add Customer";
            this.btn_AddCustomer.UseVisualStyleBackColor = true;
            this.btn_AddCustomer.Click += new System.EventHandler(this.AddCustomerToDatabase);
            // 
            // AddCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(305, 237);
            this.Controls.Add(this.panel1);
            this.Name = "AddCustomer";
            this.Text = "Customer";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtbx_PhoneNumber;
        private System.Windows.Forms.Label lbl_PhoneNumber;
        private System.Windows.Forms.TextBox txtbx_EmailAddress;
        private System.Windows.Forms.Label lbl_EmailAddress;
        private System.Windows.Forms.TextBox txtbx_ZipCode;
        private System.Windows.Forms.Label lbl_Zipcode;
        private System.Windows.Forms.TextBox txtbx_State;
        private System.Windows.Forms.Label lbl_State;
        private System.Windows.Forms.TextBox txtbx_City;
        private System.Windows.Forms.Label lbl_City;
        private System.Windows.Forms.TextBox txtbx_StreetAddress;
        private System.Windows.Forms.Label lbl_AddressStreet;
        private System.Windows.Forms.TextBox txtbx_LastName;
        private System.Windows.Forms.Label lbl_Lastname;
        private System.Windows.Forms.TextBox txtbx_FirstName;
        private System.Windows.Forms.Label lbl_FirstName;
        private System.Windows.Forms.Button btn_AddCustomer;
        private System.Windows.Forms.Button btn_DeleteCustomer;

    }
}