﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace nVoice_Express
{
    public partial class EditCompanyDetails : Form
    {
        nVoiceEntities nVoiceDB = new nVoiceEntities();

        public EditCompanyDetails()
        {
            InitializeComponent();
        }

        private void SaveCompanyDetails(object sender, EventArgs e)
        {
            var CompanyDetails = new CompanyDetail()
                                     {
                                         CompanyName = txtbx_CompanyName.Text,
                                         CompanyOwner = txtbx_CompanyOwner.Text,
                                         CompanyAddress = txtbx_CompanyAddress.Text,
                                         CompanyCity = txtbx_CompanyCity.Text,
                                         CompanyState = txtbx_CompanyState.Text,
                                         CompanyZipCode = txtbx_CompanyZipCode.Text,
                                         CompanyPhoneNumber = txtbx_CompanyPhoneNumber.Text,
                                         CompanyEmailAddress = txtbx_CompanyEmailAddress.Text,
                                         CompanyWebsite = txtbx_CompanyWebsite.Text
                                     };
            if (! nVoiceDB.CompanyDetails.Any())
            {
                nVoiceDB.CompanyDetails.Add(CompanyDetails);
                nVoiceDB.SaveChanges();
                Application.Restart();
            }
            else
            {
                var curCompanyEntry = nVoiceDB.CompanyDetails.FirstOrDefault();
                if (curCompanyEntry != null)
                {
                    curCompanyEntry.CompanyName = CompanyDetails.CompanyName;
                    curCompanyEntry.CompanyOwner = CompanyDetails.CompanyOwner;
                    curCompanyEntry.CompanyAddress = CompanyDetails.CompanyAddress;
                    curCompanyEntry.CompanyCity = CompanyDetails.CompanyCity;
                    curCompanyEntry.CompanyState = CompanyDetails.CompanyState;
                    curCompanyEntry.CompanyZipCode = CompanyDetails.CompanyZipCode;
                    curCompanyEntry.CompanyPhoneNumber = CompanyDetails.CompanyPhoneNumber;
                    curCompanyEntry.CompanyEmailAddress = CompanyDetails.CompanyEmailAddress;
                    curCompanyEntry.CompanyWebsite = CompanyDetails.CompanyWebsite;
                }
                nVoiceDB.SaveChanges();
                Application.Restart();

                
            }
        }

        public void GetCurrentCompanyDetails()
        {
            CompanyDetail company = nVoiceDB.CompanyDetails.FirstOrDefault();

            foreach (Control ctrl in this.Controls)
            {
                if (ctrl is TextBox)
                {
                    foreach (var companyItem in typeof(CompanyDetail).GetProperties())
                    {
                        if (ctrl.Name.ToLower().Contains(companyItem.Name.ToLower()))
                        {

                            // Dynamiically loop through each TextBox control and each CompanyDetails Property and match them with the coordinating item and add the text to the TextBox that matches.
                            ctrl.Text = GetPropValue(nVoiceDB.CompanyDetails.FirstOrDefault(), companyItem.Name);

                        }
                    }
                    
                    
                }
            }
        }

        public static string GetPropValue(dynamic src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

    }
}
