﻿namespace nVoice_Express
{
    partial class EditTemplates
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lkbl_TemplateName = new System.Windows.Forms.Label();
            this.txtbx_TemplateName = new System.Windows.Forms.TextBox();
            this.txtbx_TemplateHeader = new System.Windows.Forms.TextBox();
            this.lbl_Header = new System.Windows.Forms.Label();
            this.txtbx_TemplateDisclamer = new System.Windows.Forms.TextBox();
            this.lbl_Disclamer = new System.Windows.Forms.Label();
            this.txtbx_TemplateFooter = new System.Windows.Forms.TextBox();
            this.lbl_Footer = new System.Windows.Forms.Label();
            this.lbl_FontName = new System.Windows.Forms.Label();
            this.lbl_SchemeColor = new System.Windows.Forms.Label();
            this.dropdown_ColorList = new System.Windows.Forms.ComboBox();
            this.dropdown_FontsList = new System.Windows.Forms.ComboBox();
            this.btn_SaveTemplate = new System.Windows.Forms.Button();
            this.btn_DeleteTemplate = new System.Windows.Forms.Button();
            this.chkbx_UsePhotoForCompanyLogo = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_ShowBarcode = new System.Windows.Forms.Label();
            this.chkbx_ShowBarCode = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lkbl_TemplateName
            // 
            this.lkbl_TemplateName.AutoSize = true;
            this.lkbl_TemplateName.Location = new System.Drawing.Point(3, 15);
            this.lkbl_TemplateName.Name = "lkbl_TemplateName";
            this.lkbl_TemplateName.Size = new System.Drawing.Size(82, 13);
            this.lkbl_TemplateName.TabIndex = 0;
            this.lkbl_TemplateName.Text = "Template Name";
            // 
            // txtbx_TemplateName
            // 
            this.txtbx_TemplateName.Location = new System.Drawing.Point(119, 8);
            this.txtbx_TemplateName.Name = "txtbx_TemplateName";
            this.txtbx_TemplateName.Size = new System.Drawing.Size(278, 20);
            this.txtbx_TemplateName.TabIndex = 1;
            this.txtbx_TemplateName.TextChanged += new System.EventHandler(this.CheckForMatchingTemplateName);
            // 
            // txtbx_TemplateHeader
            // 
            this.txtbx_TemplateHeader.Location = new System.Drawing.Point(119, 30);
            this.txtbx_TemplateHeader.Multiline = true;
            this.txtbx_TemplateHeader.Name = "txtbx_TemplateHeader";
            this.txtbx_TemplateHeader.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtbx_TemplateHeader.Size = new System.Drawing.Size(278, 49);
            this.txtbx_TemplateHeader.TabIndex = 3;
            // 
            // lbl_Header
            // 
            this.lbl_Header.AutoSize = true;
            this.lbl_Header.Location = new System.Drawing.Point(3, 37);
            this.lbl_Header.Name = "lbl_Header";
            this.lbl_Header.Size = new System.Drawing.Size(110, 13);
            this.lbl_Header.TabIndex = 2;
            this.lbl_Header.Text = "Template Header Info";
            // 
            // txtbx_TemplateDisclamer
            // 
            this.txtbx_TemplateDisclamer.Location = new System.Drawing.Point(119, 141);
            this.txtbx_TemplateDisclamer.Multiline = true;
            this.txtbx_TemplateDisclamer.Name = "txtbx_TemplateDisclamer";
            this.txtbx_TemplateDisclamer.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtbx_TemplateDisclamer.Size = new System.Drawing.Size(278, 49);
            this.txtbx_TemplateDisclamer.TabIndex = 7;
            // 
            // lbl_Disclamer
            // 
            this.lbl_Disclamer.AutoSize = true;
            this.lbl_Disclamer.Location = new System.Drawing.Point(3, 148);
            this.lbl_Disclamer.Name = "lbl_Disclamer";
            this.lbl_Disclamer.Size = new System.Drawing.Size(100, 13);
            this.lbl_Disclamer.TabIndex = 6;
            this.lbl_Disclamer.Text = "Template Disclamer";
            // 
            // txtbx_TemplateFooter
            // 
            this.txtbx_TemplateFooter.Location = new System.Drawing.Point(119, 85);
            this.txtbx_TemplateFooter.Multiline = true;
            this.txtbx_TemplateFooter.Name = "txtbx_TemplateFooter";
            this.txtbx_TemplateFooter.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtbx_TemplateFooter.Size = new System.Drawing.Size(278, 50);
            this.txtbx_TemplateFooter.TabIndex = 5;
            // 
            // lbl_Footer
            // 
            this.lbl_Footer.AutoSize = true;
            this.lbl_Footer.Location = new System.Drawing.Point(3, 92);
            this.lbl_Footer.Name = "lbl_Footer";
            this.lbl_Footer.Size = new System.Drawing.Size(105, 13);
            this.lbl_Footer.TabIndex = 4;
            this.lbl_Footer.Text = "Template Footer Info";
            // 
            // lbl_FontName
            // 
            this.lbl_FontName.AutoSize = true;
            this.lbl_FontName.Location = new System.Drawing.Point(3, 225);
            this.lbl_FontName.Name = "lbl_FontName";
            this.lbl_FontName.Size = new System.Drawing.Size(59, 13);
            this.lbl_FontName.TabIndex = 10;
            this.lbl_FontName.Text = "Font Name";
            // 
            // lbl_SchemeColor
            // 
            this.lbl_SchemeColor.AutoSize = true;
            this.lbl_SchemeColor.Location = new System.Drawing.Point(3, 203);
            this.lbl_SchemeColor.Name = "lbl_SchemeColor";
            this.lbl_SchemeColor.Size = new System.Drawing.Size(73, 13);
            this.lbl_SchemeColor.TabIndex = 8;
            this.lbl_SchemeColor.Text = "Scheme Color";
            // 
            // dropdown_ColorList
            // 
            this.dropdown_ColorList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dropdown_ColorList.FormattingEnabled = true;
            this.dropdown_ColorList.Location = new System.Drawing.Point(119, 196);
            this.dropdown_ColorList.Name = "dropdown_ColorList";
            this.dropdown_ColorList.Size = new System.Drawing.Size(278, 21);
            this.dropdown_ColorList.TabIndex = 12;
            // 
            // dropdown_FontsList
            // 
            this.dropdown_FontsList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dropdown_FontsList.FormattingEnabled = true;
            this.dropdown_FontsList.Location = new System.Drawing.Point(119, 221);
            this.dropdown_FontsList.Name = "dropdown_FontsList";
            this.dropdown_FontsList.Size = new System.Drawing.Size(278, 21);
            this.dropdown_FontsList.TabIndex = 13;
            // 
            // btn_SaveTemplate
            // 
            this.btn_SaveTemplate.Location = new System.Drawing.Point(294, 298);
            this.btn_SaveTemplate.Name = "btn_SaveTemplate";
            this.btn_SaveTemplate.Size = new System.Drawing.Size(103, 23);
            this.btn_SaveTemplate.TabIndex = 14;
            this.btn_SaveTemplate.Text = "&Add Template";
            this.btn_SaveTemplate.UseVisualStyleBackColor = true;
            this.btn_SaveTemplate.Click += new System.EventHandler(this.SaveUpdateTemplate);
            // 
            // btn_DeleteTemplate
            // 
            this.btn_DeleteTemplate.ForeColor = System.Drawing.Color.DarkRed;
            this.btn_DeleteTemplate.Location = new System.Drawing.Point(185, 298);
            this.btn_DeleteTemplate.Name = "btn_DeleteTemplate";
            this.btn_DeleteTemplate.Size = new System.Drawing.Size(103, 23);
            this.btn_DeleteTemplate.TabIndex = 15;
            this.btn_DeleteTemplate.Text = "Delete Template";
            this.btn_DeleteTemplate.UseVisualStyleBackColor = true;
            this.btn_DeleteTemplate.Visible = false;
            this.btn_DeleteTemplate.Click += new System.EventHandler(this.btn_DeleteTemplate_Click);
            // 
            // chkbx_UsePhotoForCompanyLogo
            // 
            this.chkbx_UsePhotoForCompanyLogo.AutoSize = true;
            this.chkbx_UsePhotoForCompanyLogo.Location = new System.Drawing.Point(173, 249);
            this.chkbx_UsePhotoForCompanyLogo.Name = "chkbx_UsePhotoForCompanyLogo";
            this.chkbx_UsePhotoForCompanyLogo.Size = new System.Drawing.Size(15, 14);
            this.chkbx_UsePhotoForCompanyLogo.TabIndex = 16;
            this.chkbx_UsePhotoForCompanyLogo.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 248);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Use Photo for Company Logo";
            // 
            // lbl_ShowBarcode
            // 
            this.lbl_ShowBarcode.AutoSize = true;
            this.lbl_ShowBarcode.Location = new System.Drawing.Point(3, 267);
            this.lbl_ShowBarcode.Name = "lbl_ShowBarcode";
            this.lbl_ShowBarcode.Size = new System.Drawing.Size(119, 13);
            this.lbl_ShowBarcode.TabIndex = 19;
            this.lbl_ShowBarcode.Text = "Show Invoice Bar Code";
            // 
            // chkbx_ShowBarCode
            // 
            this.chkbx_ShowBarCode.AutoSize = true;
            this.chkbx_ShowBarCode.Location = new System.Drawing.Point(173, 268);
            this.chkbx_ShowBarCode.Name = "chkbx_ShowBarCode";
            this.chkbx_ShowBarCode.Size = new System.Drawing.Size(15, 14);
            this.chkbx_ShowBarCode.TabIndex = 18;
            this.chkbx_ShowBarCode.UseVisualStyleBackColor = true;
            // 
            // EditTemplates
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 325);
            this.Controls.Add(this.lbl_ShowBarcode);
            this.Controls.Add(this.chkbx_ShowBarCode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkbx_UsePhotoForCompanyLogo);
            this.Controls.Add(this.btn_DeleteTemplate);
            this.Controls.Add(this.btn_SaveTemplate);
            this.Controls.Add(this.dropdown_FontsList);
            this.Controls.Add(this.dropdown_ColorList);
            this.Controls.Add(this.lbl_FontName);
            this.Controls.Add(this.lbl_SchemeColor);
            this.Controls.Add(this.txtbx_TemplateDisclamer);
            this.Controls.Add(this.lbl_Disclamer);
            this.Controls.Add(this.txtbx_TemplateFooter);
            this.Controls.Add(this.lbl_Footer);
            this.Controls.Add(this.txtbx_TemplateHeader);
            this.Controls.Add(this.lbl_Header);
            this.Controls.Add(this.txtbx_TemplateName);
            this.Controls.Add(this.lkbl_TemplateName);
            this.Name = "EditTemplates";
            this.Text = "Templates";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lkbl_TemplateName;
        private System.Windows.Forms.TextBox txtbx_TemplateName;
        private System.Windows.Forms.TextBox txtbx_TemplateHeader;
        private System.Windows.Forms.Label lbl_Header;
        private System.Windows.Forms.TextBox txtbx_TemplateDisclamer;
        private System.Windows.Forms.Label lbl_Disclamer;
        private System.Windows.Forms.TextBox txtbx_TemplateFooter;
        private System.Windows.Forms.Label lbl_Footer;
        private System.Windows.Forms.Label lbl_FontName;
        private System.Windows.Forms.Label lbl_SchemeColor;
        private System.Windows.Forms.ComboBox dropdown_ColorList;
        private System.Windows.Forms.ComboBox dropdown_FontsList;
        private System.Windows.Forms.Button btn_SaveTemplate;
        internal System.Windows.Forms.Button btn_DeleteTemplate;
        private System.Windows.Forms.CheckBox chkbx_UsePhotoForCompanyLogo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_ShowBarcode;
        private System.Windows.Forms.CheckBox chkbx_ShowBarCode;
    }
}