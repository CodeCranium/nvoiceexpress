﻿namespace nVoice_Express
{
    partial class AddServices
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_TypeOfService = new System.Windows.Forms.Label();
            this.txtbx_TypeOfService = new System.Windows.Forms.TextBox();
            this.txtbx_DescriptionOfService = new System.Windows.Forms.TextBox();
            this.lbl_DescriptionOfService = new System.Windows.Forms.Label();
            this.txtbx_AdditionalNotes = new System.Windows.Forms.TextBox();
            this.lbl_AdditionalNotes = new System.Windows.Forms.Label();
            this.txtbx_Price = new System.Windows.Forms.TextBox();
            this.lbl_Price = new System.Windows.Forms.Label();
            this.btn_AddEditService = new System.Windows.Forms.Button();
            this.btn_DeleteService = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_TypeOfService
            // 
            this.lbl_TypeOfService.AutoSize = true;
            this.lbl_TypeOfService.Location = new System.Drawing.Point(12, 13);
            this.lbl_TypeOfService.Name = "lbl_TypeOfService";
            this.lbl_TypeOfService.Size = new System.Drawing.Size(82, 13);
            this.lbl_TypeOfService.TabIndex = 0;
            this.lbl_TypeOfService.Text = "Type of Service";
            // 
            // txtbx_TypeOfService
            // 
            this.txtbx_TypeOfService.Location = new System.Drawing.Point(102, 6);
            this.txtbx_TypeOfService.MaxLength = 120;
            this.txtbx_TypeOfService.Name = "txtbx_TypeOfService";
            this.txtbx_TypeOfService.Size = new System.Drawing.Size(262, 20);
            this.txtbx_TypeOfService.TabIndex = 1;
            this.txtbx_TypeOfService.TextChanged += new System.EventHandler(this.checkForMatchingService);
            // 
            // txtbx_DescriptionOfService
            // 
            this.txtbx_DescriptionOfService.Location = new System.Drawing.Point(102, 28);
            this.txtbx_DescriptionOfService.Name = "txtbx_DescriptionOfService";
            this.txtbx_DescriptionOfService.Size = new System.Drawing.Size(262, 20);
            this.txtbx_DescriptionOfService.TabIndex = 3;
            // 
            // lbl_DescriptionOfService
            // 
            this.lbl_DescriptionOfService.AutoSize = true;
            this.lbl_DescriptionOfService.Location = new System.Drawing.Point(12, 35);
            this.lbl_DescriptionOfService.Name = "lbl_DescriptionOfService";
            this.lbl_DescriptionOfService.Size = new System.Drawing.Size(60, 13);
            this.lbl_DescriptionOfService.TabIndex = 2;
            this.lbl_DescriptionOfService.Text = "Description";
            // 
            // txtbx_AdditionalNotes
            // 
            this.txtbx_AdditionalNotes.Location = new System.Drawing.Point(102, 49);
            this.txtbx_AdditionalNotes.Name = "txtbx_AdditionalNotes";
            this.txtbx_AdditionalNotes.Size = new System.Drawing.Size(262, 20);
            this.txtbx_AdditionalNotes.TabIndex = 5;
            // 
            // lbl_AdditionalNotes
            // 
            this.lbl_AdditionalNotes.AutoSize = true;
            this.lbl_AdditionalNotes.Location = new System.Drawing.Point(12, 56);
            this.lbl_AdditionalNotes.Name = "lbl_AdditionalNotes";
            this.lbl_AdditionalNotes.Size = new System.Drawing.Size(84, 13);
            this.lbl_AdditionalNotes.TabIndex = 4;
            this.lbl_AdditionalNotes.Text = "Additional Notes";
            // 
            // txtbx_Price
            // 
            this.txtbx_Price.Location = new System.Drawing.Point(102, 71);
            this.txtbx_Price.MaxLength = 100;
            this.txtbx_Price.Name = "txtbx_Price";
            this.txtbx_Price.Size = new System.Drawing.Size(262, 20);
            this.txtbx_Price.TabIndex = 7;
            // 
            // lbl_Price
            // 
            this.lbl_Price.AutoSize = true;
            this.lbl_Price.Location = new System.Drawing.Point(12, 78);
            this.lbl_Price.Name = "lbl_Price";
            this.lbl_Price.Size = new System.Drawing.Size(31, 13);
            this.lbl_Price.TabIndex = 6;
            this.lbl_Price.Text = "Price";
            // 
            // btn_AddEditService
            // 
            this.btn_AddEditService.Location = new System.Drawing.Point(289, 108);
            this.btn_AddEditService.Name = "btn_AddEditService";
            this.btn_AddEditService.Size = new System.Drawing.Size(75, 23);
            this.btn_AddEditService.TabIndex = 8;
            this.btn_AddEditService.Text = "&Add Service";
            this.btn_AddEditService.UseVisualStyleBackColor = true;
            this.btn_AddEditService.Click += new System.EventHandler(this.AddService);
            // 
            // btn_DeleteService
            // 
            this.btn_DeleteService.ForeColor = System.Drawing.Color.DarkRed;
            this.btn_DeleteService.Location = new System.Drawing.Point(192, 108);
            this.btn_DeleteService.Name = "btn_DeleteService";
            this.btn_DeleteService.Size = new System.Drawing.Size(91, 23);
            this.btn_DeleteService.TabIndex = 9;
            this.btn_DeleteService.Text = "Delete Service";
            this.btn_DeleteService.UseVisualStyleBackColor = true;
            this.btn_DeleteService.Visible = false;
            this.btn_DeleteService.Click += new System.EventHandler(this.btn_DeleteService_Click);
            // 
            // AddServices
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 141);
            this.Controls.Add(this.btn_DeleteService);
            this.Controls.Add(this.btn_AddEditService);
            this.Controls.Add(this.txtbx_Price);
            this.Controls.Add(this.lbl_Price);
            this.Controls.Add(this.txtbx_AdditionalNotes);
            this.Controls.Add(this.lbl_AdditionalNotes);
            this.Controls.Add(this.txtbx_DescriptionOfService);
            this.Controls.Add(this.lbl_DescriptionOfService);
            this.Controls.Add(this.txtbx_TypeOfService);
            this.Controls.Add(this.lbl_TypeOfService);
            this.Name = "AddServices";
            this.Text = "Services";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_TypeOfService;
        private System.Windows.Forms.TextBox txtbx_TypeOfService;
        private System.Windows.Forms.TextBox txtbx_DescriptionOfService;
        private System.Windows.Forms.Label lbl_DescriptionOfService;
        private System.Windows.Forms.TextBox txtbx_AdditionalNotes;
        private System.Windows.Forms.Label lbl_AdditionalNotes;
        private System.Windows.Forms.TextBox txtbx_Price;
        private System.Windows.Forms.Label lbl_Price;
        private System.Windows.Forms.Button btn_AddEditService;
        private System.Windows.Forms.Button btn_DeleteService;
    }
}