﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Drawing.BarCodes;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;

namespace nVoice_Express
{
    class CreatePdf
    {

        internal void CreateAndSaveInvoice(Invoice invoiceInfo, InvoiceTemplate invoiceTemplate)
        {
            // Create a new PDF document
            PdfDocument document = new PdfDocument();
            document.Info.Title = invoiceInfo.CompanyInfo.CompanyName + " Invoice created by nVoice Express";

            // Create an empty page
            PdfPage page = document.AddPage();

            // Get an XGraphics object for drawing
            XGraphics gfx = XGraphics.FromPdfPage(page);

            // Create a font  // XFont xsmallFont = new XFont("Arial", 8, XFontStyle.Regular);
            string fontType = invoiceTemplate.FontName;
            XFont xsmallFont = new XFont(fontType, 8, XFontStyle.Regular);
            XFont smallFont = new XFont(fontType, 12, XFontStyle.Regular);
            XFont mediumFont = new XFont(fontType, 16, XFontStyle.Regular);
            XFont largeFont = new XFont(fontType, 20, XFontStyle.Regular);
            
            
            /* Draw the text to defined location in the Center of the page using xStringFormats. */
            //gfx.DrawString(GetTextForDocument(invoiceInfo), font, XBrushes.Black,
            //new XRect(0, 0, page.Width, page.Height), XStringFormats.Center);

            /* Example of drawing on page */
            /* Regular 8x11 sheet is xHorizonal=(20 to 575) & yVertical=(20 to 775) */
            // gfx.DrawString("TextInfo", font, XBrushes.Black, xHorizonal, yVertical);


            // Company Name Header
            XBrush templateColor = TemplateDetails.GetTemplateColor(templateColor: (TemplateDetails.TemplateColors)Enum.Parse(typeof(TemplateDetails.TemplateColors), invoiceTemplate.SchemeColor));
            
            /* Change BackGround color (if set in Template) */
            // gfx.DrawRectangle(XBrushes.PowderBlue, 20, 20, 575, 775);

            // Draw Top head info.
            gfx.DrawRectangle(templateColor, 20, 20, 555, 50);
            gfx.DrawString(invoiceInfo.CompanyInfo.CompanyName, largeFont, XBrushes.White, 30, 50);
            gfx.DrawString("INVOICE", largeFont, XBrushes.White, 475, 50);

            //Draw Company Logo Image (If it is True) in the template settings
            if (invoiceTemplate.UseCompanyLogoPhoto == true && Properties.Settings.Default.CompanyLogoImageJpegPath != String.Empty)
            {
                string jpgCompanyLogoFilePath = Properties.Settings.Default.CompanyLogoImageJpegPath;
                XImage image = XImage.FromFile(jpgCompanyLogoFilePath);
                gfx.DrawImage(image, 20, 20, 300, 50);
            }

            // Box for the Business Details - Top Left area
            XPoint businessDetailsBox = new XPoint(x: 20 , y:80);
            int offsetForTextHeight = 15;
            gfx.DrawRectangle(XPens.Black, businessDetailsBox.X, businessDetailsBox.Y, 200, 100);
            gfx.DrawString(invoiceInfo.CompanyInfo.CompanyOwner, smallFont, XBrushes.Black, businessDetailsBox.X + 10, businessDetailsBox.Y + offsetForTextHeight);
            gfx.DrawString(invoiceInfo.CompanyInfo.CompanyAddress, smallFont, XBrushes.Black, businessDetailsBox.X + 10, businessDetailsBox.Y + (offsetForTextHeight * 2));
            string businessCityStateZip = invoiceInfo.CompanyInfo.CompanyCity + ", " + invoiceInfo.CompanyInfo.CompanyState + " " + invoiceInfo.CompanyInfo.CompanyZipCode;
            gfx.DrawString(businessCityStateZip, smallFont, XBrushes.Black, businessDetailsBox.X + 10, businessDetailsBox.Y + (offsetForTextHeight * 3));
            gfx.DrawString(invoiceInfo.CompanyInfo.CompanyEmailAddress, smallFont, XBrushes.Black, businessDetailsBox.X + 10, businessDetailsBox.Y + (offsetForTextHeight * 4));
            gfx.DrawString(invoiceInfo.CompanyInfo.CompanyPhoneNumber, smallFont, XBrushes.Black, businessDetailsBox.X + 10, businessDetailsBox.Y + (offsetForTextHeight * 5));
            gfx.DrawString(invoiceInfo.CompanyInfo.CompanyWebsite, xsmallFont, XBrushes.Black, businessDetailsBox.X + 10, businessDetailsBox.Y + (offsetForTextHeight * 6));

            /* Loop through each Property in the CompanyInfo Object  [Alternate way to show Contact details]*/
            //int propertyPlacement = 1;
            //foreach (PropertyInfo propertyInfo in invoiceInfo.CompanyInfo.GetType().GetProperties())
            //{
            //    var propVal = propertyInfo.GetValue(invoiceInfo.CompanyInfo, null);
            //    gfx.DrawString(propVal.ToString(), smallFont, XBrushes.Green, businessDetailsBox.X + 10, businessDetailsBox.Y + (offsetForTextHeight * propertyPlacement));
            //    propertyPlacement += 1;
            //}

            //To Customer information
            var offsetCustomerLeft = businessDetailsBox.X + 320;
            gfx.DrawString(String.Format("{0} {1}", invoiceInfo.CustomerInfo.Firstname, invoiceInfo.CustomerInfo.Lastname), smallFont, XBrushes.Black, offsetCustomerLeft, businessDetailsBox.Y + offsetForTextHeight);
            gfx.DrawString(invoiceInfo.CustomerInfo.Address, smallFont, XBrushes.Black, offsetCustomerLeft, businessDetailsBox.Y + (offsetForTextHeight * 2));
            string customerCityStateZip = (invoiceInfo.CustomerInfo.City + ", " + invoiceInfo.CustomerInfo.State + " " + invoiceInfo.CustomerInfo.Zipcode).Trim(',');
            gfx.DrawString(customerCityStateZip, smallFont, XBrushes.Black, offsetCustomerLeft, businessDetailsBox.Y + (offsetForTextHeight * 3));

            gfx.DrawString(invoiceInfo.CustomerInfo.EmailAddress, smallFont, XBrushes.Black, offsetCustomerLeft, businessDetailsBox.Y + (offsetForTextHeight * 5));
            gfx.DrawString(invoiceInfo.CustomerInfo.PhoneNumber, smallFont, XBrushes.Black, offsetCustomerLeft, businessDetailsBox.Y + (offsetForTextHeight * 6));

            //Input the Header Section
            gfx.DrawString(invoiceInfo.InvoiceHeader, smallFont, XBrushes.Black, 20, 195);

            XPoint pointServiceList = new XPoint(x: 20, y: 270);
            int cellWidth = 500;
            int cellHeight = 20;
            int offsetForPrice = 75;
            double totalCostCalculation = 0.00;

            foreach (var servicesInvoiceList in invoiceInfo.ServicesAndPricesList)
            {
                gfx.DrawRectangle(XPens.Black, pointServiceList.X, pointServiceList.Y - cellHeight, cellWidth, cellHeight);
                // List the Service
                gfx.DrawString(servicesInvoiceList.TypeOfService, smallFont, XBrushes.Black, pointServiceList.X + 2, pointServiceList.Y - 2);
                // List the Price
                gfx.DrawString(servicesInvoiceList.Price, smallFont, XBrushes.Black, ((pointServiceList.X + cellWidth) - offsetForPrice), pointServiceList.Y - 2);
                pointServiceList.Y += 20;  // Goto Next Line
                totalCostCalculation += double.Parse(servicesInvoiceList.Price, NumberStyles.Currency);
            }

            // Add Total Price as last line
            gfx.DrawRectangle(XPens.Black, pointServiceList.X, pointServiceList.Y - cellHeight, cellWidth, cellHeight);
            gfx.DrawString("Total Cost", smallFont, XBrushes.Black, pointServiceList.X + 2, pointServiceList.Y - 2);
            gfx.DrawString("$" + totalCostCalculation.ToString("N2"), smallFont, XBrushes.Red, ((pointServiceList.X + cellWidth) - offsetForPrice), pointServiceList.Y - 2);
                // .ToString("N2") converts to decimal with currency style formatting.

            //Input the footer section [ Word-Wrap the data every 100 characters ]
            int startPointValueOfFooter = (int) (pointServiceList.Y + 50);
            int maxCharactersBeforeWordWrap = 100;
            int numberOfLinesInFooter = 1;
            if (invoiceInfo.InvoiceFooter.Length >= maxCharactersBeforeWordWrap)
            {
               var formattedInvoiceFooter = WordWrap.SplitAt(invoiceInfo.InvoiceFooter.ToString(), maxCharactersBeforeWordWrap);
               
               foreach (var splitFooter in formattedInvoiceFooter)
	           {
		           gfx.DrawString(splitFooter, smallFont, XBrushes.Black, pointServiceList.X, startPointValueOfFooter + (20 * numberOfLinesInFooter));
	               numberOfLinesInFooter += 1;
	           } 
            }
            
            //Input the Disclamer bottm section
            int startPointValueOfDisclamer = startPointValueOfFooter + (numberOfLinesInFooter * 20);
            string Disclamer = invoiceInfo.InvoiceDisclamer != null ? invoiceInfo.InvoiceDisclamer : String.Empty;
            gfx.DrawString(Disclamer, xsmallFont, XBrushes.Gray, pointServiceList.X, startPointValueOfDisclamer);

            // Save the information to the Database for logging
            Logging savedPdfLog = new Logging();
            savedPdfLog.TypeOfEvent = Logging.EventType.SavePDF;
            savedPdfLog.InvoiceDetails = invoiceInfo;
            invoiceInfo.InvoiceIdentifier = savedPdfLog.SaveLogEventToDatabase();

            /* [Print a Bar Code] - At Bottom Right corner */
            if (invoiceTemplate.ShowBarCode == true)
            {
                var myXSize = new XSize(200, 30);
                var myNewCode = new PdfSharp.Drawing.BarCodes.Code3of9Standard(invoiceInfo.InvoiceIdentifier.ToString(), myXSize);
                var myXPoint = new XPoint(375, 725);
                gfx.DrawBarCode(myNewCode, myXPoint);
            }
            

            // Save the document...
            string filename = invoiceInfo.CustomerInfo.Firstname + invoiceInfo.CustomerInfo.Lastname + "_INVOICE.pdf";
            string saveLocation = GetSaveLocationDir(filename);
            document.Save(saveLocation);
            
            // ...and start a PDF viewer.
            // Process.Start(filename);

            //Send the saved file name back to the startup Form to use as needed.
            nVoiceStartupForm.SavedInvoiceFilePath = saveLocation;      
        }

        private string GetSaveLocationDir(string filename)
        {
            try
            {
                string dirPath = Properties.Settings.Default.DefaultFolderToSaveInvoices;
                if (!Directory.Exists(dirPath)) { Directory.CreateDirectory(dirPath); }
                if (dirPath[dirPath.Count() -1] != '\\')
                {
                    dirPath += '\\';
                }
                return dirPath + filename;
            }
            catch (Exception ex)
            {
                new ResultMessage() { ResultMessageInformation = String.Format("Default Save location for the Invoice Save location is not set correctly.\n{0}", ex.Message), ResultType = ResultMessage.resultType.Info }.Alert();
                throw new Exception("Error saving Invoice");
            }
        }
    }
}
