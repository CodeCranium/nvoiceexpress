﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nVoice_Express
{
    class Logging
    {
        nVoiceEntities nVoiceDB = new nVoiceEntities();

        internal enum EventType
        {
            SavePDF = 0,
            SendEmail = 1
        }
        
        public EventType TypeOfEvent { get; set; }
        public Invoice InvoiceDetails { get; set; }

        // Example Instance:  new ResultMessage() { ResultMessageInformation = "No customer First and Last name provided.  The First and Last name will need to be provided to identify which customer to DELETE.", ResultType = ResultMessage.resultType.Error }.Alert();
            
        public int SaveLogEventToDatabase()
        {
            int recordSavedIdentifier = new int();
            switch (TypeOfEvent)
            {
            
                case EventType.SendEmail : 
                    // Save to dbo.SentInvoices 

                    double totalPriceCalc = 0.00;
                    foreach (var listPrices in InvoiceDetails.ServicesAndPricesList)
                    {
                        totalPriceCalc += double.Parse(listPrices.Price.Remove('$'));
                    }
                   
                    SentInvoice saveSentInvoice = new SentInvoice()
                                                      {
                                                          ClientFullName = InvoiceDetails.CustomerInfo.Firstname + " " + InvoiceDetails.CustomerInfo.Lastname,
                                                          ClientFullAddress = InvoiceDetails.CustomerInfo.Address + ", " + InvoiceDetails.CustomerInfo.City + ", " + InvoiceDetails.CustomerInfo.State + " " + InvoiceDetails.CustomerInfo.Zipcode,
                                                          ClientEmailAddress = InvoiceDetails.CustomerInfo.EmailAddress,
                                                          InvoicedItems = String.Join(", ", InvoiceDetails.ServicesAndPricesList),
                                                          TotalPriceInvoiced = "$" + totalPriceCalc.ToString("N2"),
                                                          SentMethod = TypeOfEvent.ToString()
                                                      };
                    nVoiceDB.SentInvoices.Add(saveSentInvoice);
                    nVoiceDB.SaveChanges();
                    break;

                case EventType.SavePDF:

                    string listOfServicesJoined = String.Empty;
                    foreach (var servicesRendered in InvoiceDetails.ServicesAndPricesList)
                    {
                        listOfServicesJoined += "{" + servicesRendered.ServiceID + ":" + servicesRendered.TypeOfService + "}";
                    }
                    
                    string listofPricesJoined = string.Empty;
                    foreach (var servicesRendered in InvoiceDetails.ServicesAndPricesList)
                    {
                        listofPricesJoined += "{" + servicesRendered.ServiceID + ":" + servicesRendered.Price + "}";
                    }

                    //update the 'dbo.ServiceInvoice' database
                    ServiceInvoice savedInvoice = new ServiceInvoice()
                                                      {
                                                          ClientID = InvoiceDetails.CustomerInfo.ClientID,
                                                          ServiceID = listOfServicesJoined,
                                                          ServicePrice = listofPricesJoined,
                                                          Unpaid = true,
                                                          DateOfService = InvoiceDetails.DateOfService
                                                      };
                    nVoiceDB.ServiceInvoices.Add(savedInvoice);
                    nVoiceDB.SaveChanges();
                    recordSavedIdentifier = savedInvoice.InvoiceID;
                    break;
            }
            return recordSavedIdentifier;
        }

        

        // Method to capture a SavePDF or SendEmail event to log it to the SQL Database (for Logging)
    }
}
