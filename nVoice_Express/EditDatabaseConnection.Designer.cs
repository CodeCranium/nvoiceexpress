﻿namespace nVoice_Express
{
    partial class EditDatabaseConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_SqlInstanceName = new System.Windows.Forms.Label();
            this.txtbx_SqlInstanceName = new System.Windows.Forms.TextBox();
            this.lbl_conStr = new System.Windows.Forms.Label();
            this.txtbx_ConnectionString = new System.Windows.Forms.TextBox();
            this.txtbx_SqlDatabaseName = new System.Windows.Forms.TextBox();
            this.lbl_SqlDatabaseName = new System.Windows.Forms.Label();
            this.txtbx_DatabaseConnectionProvider = new System.Windows.Forms.TextBox();
            this.lbl_DatabaseConnectionProvider = new System.Windows.Forms.Label();
            this.panel_UsernamePasswordSecurity = new System.Windows.Forms.Panel();
            this.txtbx_Password = new System.Windows.Forms.TextBox();
            this.lbl_Password = new System.Windows.Forms.Label();
            this.txtbx_Username = new System.Windows.Forms.TextBox();
            this.lbl_Username = new System.Windows.Forms.Label();
            this.chkbx_SQLTrustedSecurity = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_CreateNewDatabase = new System.Windows.Forms.Button();
            this.panel_UsernamePasswordSecurity.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_SqlInstanceName
            // 
            this.lbl_SqlInstanceName.AutoSize = true;
            this.lbl_SqlInstanceName.Location = new System.Drawing.Point(12, 15);
            this.lbl_SqlInstanceName.Name = "lbl_SqlInstanceName";
            this.lbl_SqlInstanceName.Size = new System.Drawing.Size(103, 13);
            this.lbl_SqlInstanceName.TabIndex = 0;
            this.lbl_SqlInstanceName.Text = "SQL Instance Name";
            // 
            // txtbx_SqlInstanceName
            // 
            this.txtbx_SqlInstanceName.Location = new System.Drawing.Point(120, 12);
            this.txtbx_SqlInstanceName.Name = "txtbx_SqlInstanceName";
            this.txtbx_SqlInstanceName.Size = new System.Drawing.Size(268, 20);
            this.txtbx_SqlInstanceName.TabIndex = 1;
            // 
            // lbl_conStr
            // 
            this.lbl_conStr.AutoSize = true;
            this.lbl_conStr.Location = new System.Drawing.Point(12, 221);
            this.lbl_conStr.Name = "lbl_conStr";
            this.lbl_conStr.Size = new System.Drawing.Size(91, 13);
            this.lbl_conStr.TabIndex = 2;
            this.lbl_conStr.Text = "Connection String";
            this.lbl_conStr.Visible = false;
            // 
            // txtbx_ConnectionString
            // 
            this.txtbx_ConnectionString.Location = new System.Drawing.Point(123, 218);
            this.txtbx_ConnectionString.Name = "txtbx_ConnectionString";
            this.txtbx_ConnectionString.ReadOnly = true;
            this.txtbx_ConnectionString.Size = new System.Drawing.Size(268, 20);
            this.txtbx_ConnectionString.TabIndex = 3;
            this.txtbx_ConnectionString.Visible = false;
            // 
            // txtbx_SqlDatabaseName
            // 
            this.txtbx_SqlDatabaseName.Location = new System.Drawing.Point(120, 34);
            this.txtbx_SqlDatabaseName.Name = "txtbx_SqlDatabaseName";
            this.txtbx_SqlDatabaseName.Size = new System.Drawing.Size(268, 20);
            this.txtbx_SqlDatabaseName.TabIndex = 5;
            // 
            // lbl_SqlDatabaseName
            // 
            this.lbl_SqlDatabaseName.AutoSize = true;
            this.lbl_SqlDatabaseName.Location = new System.Drawing.Point(12, 37);
            this.lbl_SqlDatabaseName.Name = "lbl_SqlDatabaseName";
            this.lbl_SqlDatabaseName.Size = new System.Drawing.Size(84, 13);
            this.lbl_SqlDatabaseName.TabIndex = 4;
            this.lbl_SqlDatabaseName.Text = "Database Name";
            // 
            // txtbx_DatabaseConnectionProvider
            // 
            this.txtbx_DatabaseConnectionProvider.Location = new System.Drawing.Point(120, 56);
            this.txtbx_DatabaseConnectionProvider.Name = "txtbx_DatabaseConnectionProvider";
            this.txtbx_DatabaseConnectionProvider.ReadOnly = true;
            this.txtbx_DatabaseConnectionProvider.Size = new System.Drawing.Size(268, 20);
            this.txtbx_DatabaseConnectionProvider.TabIndex = 7;
            // 
            // lbl_DatabaseConnectionProvider
            // 
            this.lbl_DatabaseConnectionProvider.AutoSize = true;
            this.lbl_DatabaseConnectionProvider.Location = new System.Drawing.Point(12, 59);
            this.lbl_DatabaseConnectionProvider.Name = "lbl_DatabaseConnectionProvider";
            this.lbl_DatabaseConnectionProvider.Size = new System.Drawing.Size(103, 13);
            this.lbl_DatabaseConnectionProvider.TabIndex = 6;
            this.lbl_DatabaseConnectionProvider.Text = "Connection Provider";
            // 
            // panel_UsernamePasswordSecurity
            // 
            this.panel_UsernamePasswordSecurity.Controls.Add(this.txtbx_Password);
            this.panel_UsernamePasswordSecurity.Controls.Add(this.lbl_Password);
            this.panel_UsernamePasswordSecurity.Controls.Add(this.txtbx_Username);
            this.panel_UsernamePasswordSecurity.Controls.Add(this.lbl_Username);
            this.panel_UsernamePasswordSecurity.Location = new System.Drawing.Point(15, 134);
            this.panel_UsernamePasswordSecurity.Name = "panel_UsernamePasswordSecurity";
            this.panel_UsernamePasswordSecurity.Size = new System.Drawing.Size(376, 56);
            this.panel_UsernamePasswordSecurity.TabIndex = 10;
            this.panel_UsernamePasswordSecurity.Visible = false;
            // 
            // txtbx_Password
            // 
            this.txtbx_Password.Location = new System.Drawing.Point(128, 27);
            this.txtbx_Password.Name = "txtbx_Password";
            this.txtbx_Password.Size = new System.Drawing.Size(239, 20);
            this.txtbx_Password.TabIndex = 9;
            // 
            // lbl_Password
            // 
            this.lbl_Password.AutoSize = true;
            this.lbl_Password.Location = new System.Drawing.Point(20, 30);
            this.lbl_Password.Name = "lbl_Password";
            this.lbl_Password.Size = new System.Drawing.Size(53, 13);
            this.lbl_Password.TabIndex = 8;
            this.lbl_Password.Text = "Password";
            // 
            // txtbx_Username
            // 
            this.txtbx_Username.Location = new System.Drawing.Point(128, 5);
            this.txtbx_Username.Name = "txtbx_Username";
            this.txtbx_Username.Size = new System.Drawing.Size(239, 20);
            this.txtbx_Username.TabIndex = 7;
            // 
            // lbl_Username
            // 
            this.lbl_Username.AutoSize = true;
            this.lbl_Username.Location = new System.Drawing.Point(20, 8);
            this.lbl_Username.Name = "lbl_Username";
            this.lbl_Username.Size = new System.Drawing.Size(55, 13);
            this.lbl_Username.TabIndex = 6;
            this.lbl_Username.Text = "Username";
            // 
            // chkbx_SQLTrustedSecurity
            // 
            this.chkbx_SQLTrustedSecurity.AutoSize = true;
            this.chkbx_SQLTrustedSecurity.Location = new System.Drawing.Point(15, 107);
            this.chkbx_SQLTrustedSecurity.Name = "chkbx_SQLTrustedSecurity";
            this.chkbx_SQLTrustedSecurity.Size = new System.Drawing.Size(214, 17);
            this.chkbx_SQLTrustedSecurity.TabIndex = 0;
            this.chkbx_SQLTrustedSecurity.Text = "Trusted Security (SQL Uses Login User]";
            this.chkbx_SQLTrustedSecurity.UseVisualStyleBackColor = true;
            this.chkbx_SQLTrustedSecurity.Visible = false;
            this.chkbx_SQLTrustedSecurity.CheckedChanged += new System.EventHandler(this.ShowHideSecUsernamePasswordOption);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(293, 84);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(95, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "Save Settings";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.SaveConnectionSettings);
            // 
            // btn_CreateNewDatabase
            // 
            this.btn_CreateNewDatabase.Location = new System.Drawing.Point(15, 84);
            this.btn_CreateNewDatabase.Name = "btn_CreateNewDatabase";
            this.btn_CreateNewDatabase.Size = new System.Drawing.Size(129, 23);
            this.btn_CreateNewDatabase.TabIndex = 12;
            this.btn_CreateNewDatabase.Text = "Create New Database";
            this.btn_CreateNewDatabase.UseVisualStyleBackColor = true;
            this.btn_CreateNewDatabase.Click += new System.EventHandler(this.btn_CreateNewDatabase_Click);
            // 
            // EditDatabaseConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(413, 114);
            this.Controls.Add(this.btn_CreateNewDatabase);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel_UsernamePasswordSecurity);
            this.Controls.Add(this.chkbx_SQLTrustedSecurity);
            this.Controls.Add(this.txtbx_DatabaseConnectionProvider);
            this.Controls.Add(this.lbl_DatabaseConnectionProvider);
            this.Controls.Add(this.txtbx_SqlDatabaseName);
            this.Controls.Add(this.lbl_SqlDatabaseName);
            this.Controls.Add(this.txtbx_ConnectionString);
            this.Controls.Add(this.lbl_conStr);
            this.Controls.Add(this.txtbx_SqlInstanceName);
            this.Controls.Add(this.lbl_SqlInstanceName);
            this.Name = "EditDatabaseConnection";
            this.Text = "EditDatabaseConnection";
            this.Load += new System.EventHandler(this.EditDatabaseConnection_Load);
            this.panel_UsernamePasswordSecurity.ResumeLayout(false);
            this.panel_UsernamePasswordSecurity.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_SqlInstanceName;
        private System.Windows.Forms.TextBox txtbx_SqlInstanceName;
        private System.Windows.Forms.Label lbl_conStr;
        private System.Windows.Forms.TextBox txtbx_ConnectionString;
        private System.Windows.Forms.TextBox txtbx_SqlDatabaseName;
        private System.Windows.Forms.Label lbl_SqlDatabaseName;
        private System.Windows.Forms.TextBox txtbx_DatabaseConnectionProvider;
        private System.Windows.Forms.Label lbl_DatabaseConnectionProvider;
        private System.Windows.Forms.Panel panel_UsernamePasswordSecurity;
        private System.Windows.Forms.CheckBox chkbx_SQLTrustedSecurity;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtbx_Password;
        private System.Windows.Forms.Label lbl_Password;
        private System.Windows.Forms.TextBox txtbx_Username;
        private System.Windows.Forms.Label lbl_Username;
        private System.Windows.Forms.Button btn_CreateNewDatabase;
    }
}