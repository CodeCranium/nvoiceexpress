﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;


namespace nVoice_Express
{
    public partial class nVoiceStartupForm : Form
    {
        public static string SavedInvoiceFilePath { get; set; }
        public nVoiceEntities nVoiceDB = new nVoiceEntities();

        public nVoiceStartupForm()
        {
            if (!InitialSettingsCheck())
            {
                // Gives the ability to change the Database settings without having Errors trying to load Program if DB Connection errors occur.
                this.WindowState = FormWindowState.Minimized;
                return;
            }
            
            CheckForCompanyDetails();

            InitializeComponent();

            datetime_DateOfService.Value = DateTime.Today;
            LoadUpdatedData();
            this.Focus();
        }

        public void LoadUpdatedData()
        {
            GetListUsers();
            GetListServices();
            GetListTemplates();
        }

        private void GetListTemplates()
        {

            foreach (var template in nVoiceDB.InvoiceTemplates)
            {
                dropdown_TemplateChooser.Items.Add(template.TemplateName);
            }

            if (dropdown_TemplateChooser.Items.Count != 0)
            {
                dropdown_TemplateChooser.SelectedItem = dropdown_TemplateChooser.Items[0];
            }
        }

        private void GetListServices()
        {
            foreach (var service in nVoiceDB.Services)
            {
                listbx_ServicesAvailable.Items.Add(service.TypeOfService);

            }
        }

        private void GetListUsers()
        {
            foreach (var client in nVoiceDB.Clients)
            {
                dropdown_ClientsList.Items.Add(client.Lastname + ", " + client.Firstname);
                // Need to set a value for the ClientID to use for later
            }
        }

        private void AddServiceToInvoiceItem(object sender, EventArgs e)
        {
            foreach (var selectedServices in listbx_ServicesAvailable.SelectedItems)
            {
                listbx_InvoicedServices.Items.Add(selectedServices.ToString());
            }
        }

        private void RemoveServiceToInvoiceItem(object sender, EventArgs e)
        {
           if (listbx_InvoicedServices.SelectedIndex != -1)
           {
               var itemSelected = listbx_InvoicedServices.SelectedIndex;
               listbx_InvoicedServices.Items.RemoveAt(itemSelected); 
           }
        }

        private void ProcessInvoice(object sender, EventArgs e)
        {
            try
            {
                Client c = GetCustomerDetailsFromForm();
                Invoice i = CreateInvoice(c);
            
                CreatePdf printInv = new CreatePdf();
                string chosenTemplate = dropdown_TemplateChooser.SelectedItem.ToString();
                printInv.CreateAndSaveInvoice(i, nVoiceDB.InvoiceTemplates.FirstOrDefault(x => x.TemplateName == chosenTemplate));

                // Enable the ability from the Toolstrip Menu to Print and Email the Invoice since the property value of the file name is available in  [SavedInvoiceFilePath]
                emailToolStripMenuItem.Enabled = true;
                openInvoiceToolStripMenuItem.Enabled = true;
                picbtn_EmailClient.Visible = true;

                var openCreatedInvoice = MessageBox.Show(String.Format("Invoice has been processed and saved successfully. \n Saved File: {0}", SavedInvoiceFilePath), "Saved Invoice", MessageBoxButtons.YesNo);
                if (openCreatedInvoice == DialogResult.Yes)
                {
                    System.Threading.Thread.Sleep(100);
                    Process.Start(SavedInvoiceFilePath);
                }
            }
            catch (Exception ex)
            {
                new ResultMessage() { ResultMessageInformation = String.Format(ex.Message + "\n " + ex.GetBaseException()), ResultType = ResultMessage.resultType.Error }.Alert();
            }
        }

       private Client GetCustomerDetailsFromForm()
       {
            try
            {
                var parseClientName = dropdown_ClientsList.SelectedItem.ToString().Split(',');
                string clientFName = parseClientName[1].Trim();
                string clientLName = parseClientName[0].Trim();
                var customerPicked = from x in nVoiceDB.Clients
                                      where ((x.Firstname == clientFName) && (x.Lastname == clientLName))
                                      select x;

                if (customerPicked.FirstOrDefault() != null)
                {
                    return customerPicked.FirstOrDefault();
                }
                return null;

            }
            catch (NullReferenceException noSelectedContact)
            {
                //new ResultMessage(){ResultMessageInformation = String.Format("Invalid client chosen from the customer list.\n Please choose a contact and reprocess. \n {0}", noSelectedContact.Message), ResultType = ResultMessage.resultType.Error}.Alert();
                dropdown_ClientsList.Focus();
                throw new Exception(String.Format("Invalid client chosen from the customer list.\n Please choose a contact and reprocess. \n {0}", noSelectedContact.Message));
            }
       }

        private Invoice CreateInvoice(Client customer)
        {
            string selectedTemplateName = dropdown_TemplateChooser.SelectedItem.ToString();
            var invoiceTemplate = from x in nVoiceDB.InvoiceTemplates
                                  where x.TemplateName == selectedTemplateName
                                  select x;

            var companyInfo = nVoiceDB.CompanyDetails.FirstOrDefault();
                              

            if (invoiceTemplate.FirstOrDefault() != null)
            {
                Invoice i = new Invoice()
                                {
                                    CustomerInfo = customer,
                                    ServicesAndPricesList = GetListOfServicesToInvoice(),
                                    InvoiceHeader = invoiceTemplate.FirstOrDefault().Header,
                                    InvoiceBody = "/* Details for the Prices and Services of Invoice */",
                                    InvoiceDisclamer = invoiceTemplate.FirstOrDefault().Disclamer,
                                    InvoiceFooter = invoiceTemplate.FirstOrDefault().Footer,
                                    CompanyInfo = companyInfo,
                                    DateOfService = datetime_DateOfService.Value
                                };
                return i;
            }
            return null;
        }

        private List<Service> GetListOfServicesToInvoice()
        {
            List<Service> svcs = new List<Service>();

            foreach (var service in listbx_InvoicedServices.Items)
            {
                string servItem = service.ToString();
                var curService = from x in nVoiceDB.Services
                                 where x.TypeOfService == servItem
                                 select x;

                var itemFromServiceQuery = curService.FirstOrDefault();
                if (itemFromServiceQuery != null)
                    svcs.Add(new Service()
                                 {
                                     ServiceID = itemFromServiceQuery.ServiceID,
                                     TypeOfService = itemFromServiceQuery.TypeOfService,
                                     DescriptionOfService = itemFromServiceQuery.DescriptionOfService,
                                     AdditionalNotes = itemFromServiceQuery.AdditionalNotes,
                                     Price = itemFromServiceQuery.Price,
                                 });
            }

            return svcs;
        }

        private void LoadAddCustomerWindow(object sender, EventArgs e)
        {
            AddCustomer newCustomer = new AddCustomer();
            newCustomer.Show();
        }

        
        private void EmailInvoice(object sender, EventArgs e)
        {
            if (SavedInvoiceFilePath != null)
            {
                Email sendEmail = new Email();
                sendEmail.Subject = "Invoice from " + nVoiceDB.CompanyDetails.FirstOrDefault().CompanyName;
                sendEmail.Body = "Invoice BODY";
                sendEmail.SendEmail(clientInfo: GetCustomerDetailsFromForm(), invoiceFilePath: SavedInvoiceFilePath);
            }
        }

        private void PrintInvoice(object sender, EventArgs e)
        {
            if (SavedInvoiceFilePath != null)
            {
                Process.Start(fileName: SavedInvoiceFilePath);
            }
        }

        private void EditClient(object sender, EventArgs e)
        {
            AddCustomer formEditCustomer = new AddCustomer();
            if (dropdown_ClientsList.SelectedItem != null)
            {
                formEditCustomer.GetCustomerDetails(GetCustomerDetailsFromForm());
            }
            
            formEditCustomer.Show();
        }

        private void EditServices(object sender, EventArgs e)
        {
            AddServices editServicesForm = new AddServices();
            if (listbx_ServicesAvailable.SelectedItem != null)
            {
                string chosenService = listbx_ServicesAvailable.SelectedItem.ToString();
                Service serviceChosen = nVoiceDB.Services.FirstOrDefault(x => x.TypeOfService == chosenService);
                editServicesForm.GetTemplateDetails(serviceChosen);
            }
            editServicesForm.Show();
        }

        private void EditTemplates(object sender, EventArgs e)
        {
            EditTemplates formEditTemplates = new EditTemplates();
            
            if (dropdown_TemplateChooser.SelectedItem != null)
            {
                string chosenTemplate = dropdown_TemplateChooser.SelectedItem.ToString();
                InvoiceTemplate templateChosen = nVoiceDB.InvoiceTemplates.FirstOrDefault(x => x.TemplateName == chosenTemplate);
                formEditTemplates.GetTemplateDetails(templateChosen);
            }

            formEditTemplates.Show();
        }


        private bool InitialSettingsCheck()
        {
            try
            {
                // Mock a generic connection to the DB to test connetivity. [If incorrect, it will fail].
                nVoiceDB.Services.FirstOrDefault(x => x.ServiceID >= 1);
                return true;
            }
            catch (EntityException databaseConnectionFailure)
            {
                string databaseConnection = nVoiceDB.Database.Connection.DataSource;
                string databaseName = nVoiceDB.Database.Connection.Database;
                string databaseState = nVoiceDB.Database.Connection.State.ToString();
                new ResultMessage() { ResultMessageInformation = "Database connction failed. " + Environment.NewLine + "DB Name: " + databaseName +
                    Environment.NewLine + "DB Connection: " + databaseConnection + 
                    Environment.NewLine + "DB State: " + databaseState +
                    Environment.NewLine + databaseConnectionFailure.Message, 
                    ResultType = ResultMessage.resultType.Error }.Alert();
                
                //Edit the current databse or create a new one
                EditDatabaseConnection databaseInfo = new EditDatabaseConnection();
                databaseInfo.Show();

                return false;
            }

        }

        

        private void CheckForCompanyDetails()
        {
            if (! nVoiceDB.CompanyDetails.Any())
            {
                var newCompanyResponse =
                    MessageBox.Show("It appers that the Database details are blank, or this is the first time you have run the application. \nYou will be directed to setup the necessary details for the application to run.", "First time setup.", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (newCompanyResponse == DialogResult.OK)
                {
                    EditCompanyDetails addCompany = new EditCompanyDetails();
                    addCompany.Show();
                    addCompany.Focus();
                    this.WindowState = FormWindowState.Minimized;
                }
                else
                {
                    Application.Exit();
                }
            }
        }

        private void EditCompanyDetails(object sender, EventArgs e)
        {
            EditCompanyDetails addCompany = new EditCompanyDetails();
            addCompany.GetCurrentCompanyDetails();
            addCompany.Show();
            addCompany.Focus();
        }

        private void EditDatabaseConnectionInfo(object sender, EventArgs e)
        {
            EditDatabaseConnection databaseInfo = new EditDatabaseConnection();
            databaseInfo.Show();
        }
    }
}
