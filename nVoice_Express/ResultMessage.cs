﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace nVoice_Express
{
    public class ResultMessage
    {
        public resultType ResultType { get; set; }
        public string ResultMessageInformation { get; set; }
        /// public string ResultMessageTitle { get; set; }

        public enum resultType
        {
            Info=0,
            Warning=1,
            Error=2,
            Critial=3
        }

        public void Alert()
        {
            MessageBox.Show(this.ResultMessageInformation, this.ResultType.ToString(), MessageBoxButtons.OK);
        }

    }
}
