﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace nVoice_Express
{
    public partial class AddServices : Form
    {
        public nVoiceEntities nVoiceDB = new nVoiceEntities();

        public AddServices()
        {
            InitializeComponent();
        }

        private void AddService(object sender, EventArgs e)
        {
            try
            {

            
                if (!CheckToEnsureValuesPresent(txtbx_Price))
                {
                      throw new Exception("No price specified.");
                }
            
                Service newService = new Service()
                                         {
                                             TypeOfService = txtbx_TypeOfService.Text,
                                             DescriptionOfService = txtbx_DescriptionOfService.Text,
                                             AdditionalNotes = txtbx_AdditionalNotes.Text,
                                             Price = txtbx_Price.Text
                                         };

                IQueryable<Service> findDuplicateService = from x in nVoiceDB.Services
                                                           where x.TypeOfService == txtbx_TypeOfService.Text
                                                           select x;

                Service dupItem = findDuplicateService.FirstOrDefault();
                if (dupItem != null)
                {
                    // Duplicate Service found with the same Type name [Save to existing record - Update the instance with the new info]
                    dupItem.DescriptionOfService = txtbx_DescriptionOfService.Text;
                    dupItem.AdditionalNotes = txtbx_AdditionalNotes.Text;
                    {
                        double priceConvert = double.Parse(txtbx_Price.Text.Trim().TrimStart('$'));
                        dupItem.Price = priceConvert.ToString("C");
                    }
                }
                else
                {
                    // Save new instance of newly created Service
                    nVoiceDB.Services.Add(newService);
                }

                nVoiceDB.SaveChanges(); //Save changes to Database

                // Refresh Startup Form to get the newest results
   
                this.Close();
            }
            catch (Exception ex)
            {
                new ResultMessage() { ResultMessageInformation = ex.Message, ResultType = ResultMessage.resultType.Error }.Alert();
                txtbx_Price.Focus();
            }
        }

        private bool CheckToEnsureValuesPresent(TextBox txtbx)
        {
            if (txtbx.Text != String.Empty)
            {
                return true;

            }
            return false; 
        }

        private void btn_DeleteService_Click(object sender, EventArgs e)
        {
            var serviceToDelete = (from x in nVoiceDB.Services
                                   where x.TypeOfService == txtbx_TypeOfService.Text
                                   select x).FirstOrDefault();

            if (serviceToDelete != null)
            {
                nVoiceDB.Services.Remove(serviceToDelete);
                nVoiceDB.SaveChanges();
            }
            else
            {
                new ResultMessage() { ResultMessageInformation = "No Service Type name provided.  The Service Type name will need to be provided to identify which service to DELETE.", ResultType = ResultMessage.resultType.Error }.Alert();
                txtbx_TypeOfService.Focus();
            }
        }

        private void checkForMatchingService(object sender, EventArgs e)
        {
            foreach (var serviceName in nVoiceDB.Services)
            {
                if (this.txtbx_TypeOfService.Text.ToLower() == serviceName.TypeOfService.ToLower())
                {
                    btn_DeleteService.Visible = true;
                    btn_AddEditService.Text = "&Edit Service";
                    break;
                }
                else
                {
                    btn_DeleteService.Visible = false;
                    btn_AddEditService.Text = "&Add Service";
                }
            }
        }

        public void GetTemplateDetails(Service serviceChosen)
        {
            txtbx_TypeOfService.Text = serviceChosen.TypeOfService;
            txtbx_DescriptionOfService.Text = serviceChosen.DescriptionOfService;
            txtbx_AdditionalNotes.Text = serviceChosen.AdditionalNotes;
            txtbx_Price.Text = serviceChosen.Price;
        }
    }
}
