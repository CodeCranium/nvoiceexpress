﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nVoice_Express
{
    class Invoice
    {
        public int InvoiceIdentifier { get; set; }
        public string InvoiceHeader { get; set; }
        public string InvoiceFooter { get; set; }
        public string InvoiceDisclamer { get; set; }
        public string InvoiceBody { get; set; }
        public List<Service> ServicesAndPricesList { get; set; }
        public Client CustomerInfo { get; set; }
        public CompanyDetail CompanyInfo { get; set; }
        public DateTime DateOfService { get; set; }
    }


}
