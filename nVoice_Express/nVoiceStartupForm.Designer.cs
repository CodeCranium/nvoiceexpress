﻿namespace nVoice_Express
{
    partial class nVoiceStartupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(nVoiceStartupForm));
            this.listbx_ServicesAvailable = new System.Windows.Forms.ListBox();
            this.listbx_InvoicedServices = new System.Windows.Forms.ListBox();
            this.btn_AddService = new System.Windows.Forms.Button();
            this.btn_RemoveService = new System.Windows.Forms.Button();
            this.dropdown_TemplateChooser = new System.Windows.Forms.ComboBox();
            this.lbl_Template = new System.Windows.Forms.Label();
            this.btn_ProcessInvoice = new System.Windows.Forms.Button();
            this.contextMenu_AfterProcessedOptions = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.emailInvoiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openInvoiceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addCustomerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openInvoiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editClientToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editServicesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editTemplatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editCompanyDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dropdown_ClientsList = new System.Windows.Forms.ComboBox();
            this.picbtn_EmailClient = new System.Windows.Forms.PictureBox();
            this.lbl_DateOfService = new System.Windows.Forms.Label();
            this.datetime_DateOfService = new System.Windows.Forms.DateTimePicker();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editDatabaseConnectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenu_AfterProcessedOptions.SuspendLayout();
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbtn_EmailClient)).BeginInit();
            this.SuspendLayout();
            // 
            // listbx_ServicesAvailable
            // 
            this.listbx_ServicesAvailable.FormattingEnabled = true;
            this.listbx_ServicesAvailable.Location = new System.Drawing.Point(12, 62);
            this.listbx_ServicesAvailable.Name = "listbx_ServicesAvailable";
            this.listbx_ServicesAvailable.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listbx_ServicesAvailable.Size = new System.Drawing.Size(212, 95);
            this.listbx_ServicesAvailable.TabIndex = 1;
            // 
            // listbx_InvoicedServices
            // 
            this.listbx_InvoicedServices.FormattingEnabled = true;
            this.listbx_InvoicedServices.Location = new System.Drawing.Point(258, 62);
            this.listbx_InvoicedServices.Name = "listbx_InvoicedServices";
            this.listbx_InvoicedServices.Size = new System.Drawing.Size(212, 95);
            this.listbx_InvoicedServices.TabIndex = 2;
            // 
            // btn_AddService
            // 
            this.btn_AddService.Location = new System.Drawing.Point(228, 85);
            this.btn_AddService.Name = "btn_AddService";
            this.btn_AddService.Size = new System.Drawing.Size(26, 23);
            this.btn_AddService.TabIndex = 3;
            this.btn_AddService.Text = "&>";
            this.btn_AddService.UseVisualStyleBackColor = true;
            this.btn_AddService.Click += new System.EventHandler(this.AddServiceToInvoiceItem);
            // 
            // btn_RemoveService
            // 
            this.btn_RemoveService.Location = new System.Drawing.Point(228, 114);
            this.btn_RemoveService.Name = "btn_RemoveService";
            this.btn_RemoveService.Size = new System.Drawing.Size(26, 23);
            this.btn_RemoveService.TabIndex = 4;
            this.btn_RemoveService.Text = "&<";
            this.btn_RemoveService.UseVisualStyleBackColor = true;
            this.btn_RemoveService.Click += new System.EventHandler(this.RemoveServiceToInvoiceItem);
            // 
            // dropdown_TemplateChooser
            // 
            this.dropdown_TemplateChooser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dropdown_TemplateChooser.FormattingEnabled = true;
            this.dropdown_TemplateChooser.Location = new System.Drawing.Point(98, 187);
            this.dropdown_TemplateChooser.Name = "dropdown_TemplateChooser";
            this.dropdown_TemplateChooser.Size = new System.Drawing.Size(372, 21);
            this.dropdown_TemplateChooser.TabIndex = 5;
            // 
            // lbl_Template
            // 
            this.lbl_Template.AutoSize = true;
            this.lbl_Template.Location = new System.Drawing.Point(12, 190);
            this.lbl_Template.Name = "lbl_Template";
            this.lbl_Template.Size = new System.Drawing.Size(51, 13);
            this.lbl_Template.TabIndex = 6;
            this.lbl_Template.Text = "Template";
            // 
            // btn_ProcessInvoice
            // 
            this.btn_ProcessInvoice.ContextMenuStrip = this.contextMenu_AfterProcessedOptions;
            this.btn_ProcessInvoice.Location = new System.Drawing.Point(330, 224);
            this.btn_ProcessInvoice.Name = "btn_ProcessInvoice";
            this.btn_ProcessInvoice.Size = new System.Drawing.Size(91, 23);
            this.btn_ProcessInvoice.TabIndex = 8;
            this.btn_ProcessInvoice.Text = "Process Invoice";
            this.btn_ProcessInvoice.UseVisualStyleBackColor = true;
            this.btn_ProcessInvoice.Click += new System.EventHandler(this.ProcessInvoice);
            // 
            // contextMenu_AfterProcessedOptions
            // 
            this.contextMenu_AfterProcessedOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.emailInvoiceToolStripMenuItem,
            this.openInvoiceToolStripMenuItem1});
            this.contextMenu_AfterProcessedOptions.Name = "contextMenu_AfterProcessedOptions";
            this.contextMenu_AfterProcessedOptions.Size = new System.Drawing.Size(145, 48);
            // 
            // emailInvoiceToolStripMenuItem
            // 
            this.emailInvoiceToolStripMenuItem.Name = "emailInvoiceToolStripMenuItem";
            this.emailInvoiceToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.emailInvoiceToolStripMenuItem.Text = "Email Invoice";
            this.emailInvoiceToolStripMenuItem.Click += new System.EventHandler(this.EmailInvoice);
            // 
            // openInvoiceToolStripMenuItem1
            // 
            this.openInvoiceToolStripMenuItem1.Name = "openInvoiceToolStripMenuItem1";
            this.openInvoiceToolStripMenuItem1.Size = new System.Drawing.Size(144, 22);
            this.openInvoiceToolStripMenuItem1.Text = "Open Invoice";
            this.openInvoiceToolStripMenuItem1.Click += new System.EventHandler(this.PrintInvoice);
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.settingsToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(484, 24);
            this.menuStrip.TabIndex = 9;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addCustomerToolStripMenuItem,
            this.emailToolStripMenuItem,
            this.openInvoiceToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // addCustomerToolStripMenuItem
            // 
            this.addCustomerToolStripMenuItem.Name = "addCustomerToolStripMenuItem";
            this.addCustomerToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.addCustomerToolStripMenuItem.Text = "Add Customer";
            this.addCustomerToolStripMenuItem.Click += new System.EventHandler(this.LoadAddCustomerWindow);
            // 
            // emailToolStripMenuItem
            // 
            this.emailToolStripMenuItem.Enabled = false;
            this.emailToolStripMenuItem.Name = "emailToolStripMenuItem";
            this.emailToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.emailToolStripMenuItem.Text = "Email Invoice";
            this.emailToolStripMenuItem.Click += new System.EventHandler(this.EmailInvoice);
            // 
            // openInvoiceToolStripMenuItem
            // 
            this.openInvoiceToolStripMenuItem.Enabled = false;
            this.openInvoiceToolStripMenuItem.Name = "openInvoiceToolStripMenuItem";
            this.openInvoiceToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.openInvoiceToolStripMenuItem.Text = "Open Invoice";
            this.openInvoiceToolStripMenuItem.Click += new System.EventHandler(this.PrintInvoice);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editClientToolStripMenuItem,
            this.editServicesToolStripMenuItem,
            this.editTemplatesToolStripMenuItem,
            this.editCompanyDetailsToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // editClientToolStripMenuItem
            // 
            this.editClientToolStripMenuItem.Name = "editClientToolStripMenuItem";
            this.editClientToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.editClientToolStripMenuItem.Text = "Edit Client";
            this.editClientToolStripMenuItem.Click += new System.EventHandler(this.EditClient);
            // 
            // editServicesToolStripMenuItem
            // 
            this.editServicesToolStripMenuItem.Name = "editServicesToolStripMenuItem";
            this.editServicesToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.editServicesToolStripMenuItem.Text = "Edit Services";
            this.editServicesToolStripMenuItem.Click += new System.EventHandler(this.EditServices);
            // 
            // editTemplatesToolStripMenuItem
            // 
            this.editTemplatesToolStripMenuItem.Name = "editTemplatesToolStripMenuItem";
            this.editTemplatesToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.editTemplatesToolStripMenuItem.Text = "Edit Templates";
            this.editTemplatesToolStripMenuItem.Click += new System.EventHandler(this.EditTemplates);
            // 
            // editCompanyDetailsToolStripMenuItem
            // 
            this.editCompanyDetailsToolStripMenuItem.Name = "editCompanyDetailsToolStripMenuItem";
            this.editCompanyDetailsToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.editCompanyDetailsToolStripMenuItem.Text = "Edit Company Details";
            this.editCompanyDetailsToolStripMenuItem.Click += new System.EventHandler(this.EditCompanyDetails);
            // 
            // dropdown_ClientsList
            // 
            this.dropdown_ClientsList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.dropdown_ClientsList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dropdown_ClientsList.FormattingEnabled = true;
            this.dropdown_ClientsList.Location = new System.Drawing.Point(12, 33);
            this.dropdown_ClientsList.MaxDropDownItems = 10;
            this.dropdown_ClientsList.Name = "dropdown_ClientsList";
            this.dropdown_ClientsList.Size = new System.Drawing.Size(458, 21);
            this.dropdown_ClientsList.Sorted = true;
            this.dropdown_ClientsList.TabIndex = 10;
            // 
            // picbtn_EmailClient
            // 
            this.picbtn_EmailClient.Image = ((System.Drawing.Image)(resources.GetObject("picbtn_EmailClient.Image")));
            this.picbtn_EmailClient.Location = new System.Drawing.Point(427, 214);
            this.picbtn_EmailClient.Name = "picbtn_EmailClient";
            this.picbtn_EmailClient.Size = new System.Drawing.Size(43, 33);
            this.picbtn_EmailClient.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbtn_EmailClient.TabIndex = 11;
            this.picbtn_EmailClient.TabStop = false;
            this.picbtn_EmailClient.Visible = false;
            this.picbtn_EmailClient.Click += new System.EventHandler(this.EmailInvoice);
            // 
            // lbl_DateOfService
            // 
            this.lbl_DateOfService.AutoSize = true;
            this.lbl_DateOfService.Location = new System.Drawing.Point(12, 166);
            this.lbl_DateOfService.Name = "lbl_DateOfService";
            this.lbl_DateOfService.Size = new System.Drawing.Size(81, 13);
            this.lbl_DateOfService.TabIndex = 12;
            this.lbl_DateOfService.Text = "Date of Service";
            // 
            // datetime_DateOfService
            // 
            this.datetime_DateOfService.Location = new System.Drawing.Point(98, 161);
            this.datetime_DateOfService.MinDate = new System.DateTime(1950, 1, 1, 0, 0, 0, 0);
            this.datetime_DateOfService.Name = "datetime_DateOfService";
            this.datetime_DateOfService.Size = new System.Drawing.Size(200, 20);
            this.datetime_DateOfService.TabIndex = 13;
            this.datetime_DateOfService.Value = new System.DateTime(2014, 9, 29, 9, 46, 38, 0);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editDatabaseConnectionToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // editDatabaseConnectionToolStripMenuItem
            // 
            this.editDatabaseConnectionToolStripMenuItem.Name = "editDatabaseConnectionToolStripMenuItem";
            this.editDatabaseConnectionToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.editDatabaseConnectionToolStripMenuItem.Text = "Edit Database Connection";
            this.editDatabaseConnectionToolStripMenuItem.Click += new System.EventHandler(this.EditDatabaseConnectionInfo);
            // 
            // nVoiceStartupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 252);
            this.Controls.Add(this.datetime_DateOfService);
            this.Controls.Add(this.lbl_DateOfService);
            this.Controls.Add(this.picbtn_EmailClient);
            this.Controls.Add(this.dropdown_ClientsList);
            this.Controls.Add(this.btn_ProcessInvoice);
            this.Controls.Add(this.lbl_Template);
            this.Controls.Add(this.dropdown_TemplateChooser);
            this.Controls.Add(this.btn_RemoveService);
            this.Controls.Add(this.btn_AddService);
            this.Controls.Add(this.listbx_InvoicedServices);
            this.Controls.Add(this.listbx_ServicesAvailable);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Name = "nVoiceStartupForm";
            this.Text = "nVoice Express";
            this.contextMenu_AfterProcessedOptions.ResumeLayout(false);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbtn_EmailClient)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listbx_ServicesAvailable;
        private System.Windows.Forms.ListBox listbx_InvoicedServices;
        private System.Windows.Forms.Button btn_AddService;
        private System.Windows.Forms.Button btn_RemoveService;
        private System.Windows.Forms.ComboBox dropdown_TemplateChooser;
        private System.Windows.Forms.Label lbl_Template;
        private System.Windows.Forms.Button btn_ProcessInvoice;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addCustomerToolStripMenuItem;
        private System.Windows.Forms.ComboBox dropdown_ClientsList;
        private System.Windows.Forms.ToolStripMenuItem emailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openInvoiceToolStripMenuItem;
        private System.Windows.Forms.PictureBox picbtn_EmailClient;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editClientToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editServicesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editTemplatesToolStripMenuItem;
        private System.Windows.Forms.Label lbl_DateOfService;
        private System.Windows.Forms.DateTimePicker datetime_DateOfService;
        private System.Windows.Forms.ContextMenuStrip contextMenu_AfterProcessedOptions;
        private System.Windows.Forms.ToolStripMenuItem emailInvoiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openInvoiceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem editCompanyDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editDatabaseConnectionToolStripMenuItem;
    }
}

