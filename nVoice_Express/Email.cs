﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Net.Mail;
using System.Windows.Forms;

namespace nVoice_Express
{
    internal class Email
    {
        nVoiceEntities nVoiceDB = new nVoiceEntities();
        
        public string Subject { get; set; }
        public string Body { get; set; }

        public string EmailAddressOutgoing { get { return Properties.Settings.Default.EmailAddressOutgoing; }}
        public string SmtpAddress { get { return Properties.Settings.Default.SmtpAddress; } }
        public int SmtpPort { get { return Properties.Settings.Default.SmtpPort; } }
        public string SmtpUsername { get { return Properties.Settings.Default.SmtpUsername; } }
        public string SmtpPassword { get { return Properties.Settings.Default.SmtpPassword; } }
        
        public void SendEmail(Client clientInfo, string invoiceFilePath)
        {
            try
            {
                MailMessage message = new MailMessage();
                message.To.Add(clientInfo.EmailAddress);
                message.CC.Add(nVoiceDB.CompanyDetails.FirstOrDefault().CompanyEmailAddress);
                message.Subject = this.Subject;
                message.From = new MailAddress(EmailAddressOutgoing);
                message.Body = this.Body;
                message.Attachments.Add(new Attachment(invoiceFilePath));
                SmtpClient smtp = new SmtpClient(SmtpAddress);
                smtp.Port = SmtpPort;

                // If Username is present then apply network credentails
                if (this.SmtpUsername != String.Empty)
                {
                    smtp.EnableSsl = true;
                    smtp.Credentials = new NetworkCredential(userName: SmtpUsername, password: SmtpPassword);
                }

                smtp.Send(message);
            }
            catch (Exception ex)
            {
                new ResultMessage(){ResultMessageInformation = ex.Message, ResultType = ResultMessage.resultType.Error}.Alert();
            }
        }
}
}
