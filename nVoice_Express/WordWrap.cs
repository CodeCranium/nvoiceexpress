﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nVoice_Express
{
    public static class WordWrap
    {

        public static IEnumerable<string> SplitAt(string str, int chunkSize)
        {
            List<string> newWordWrap = new List<string>();

            var wordWrapResults = Enumerable.Range(0, str.Length / chunkSize)
                .Select(i => str.Substring(i * chunkSize, chunkSize));

            string addToBeginingOfNext = String.Empty;
            foreach (var item in wordWrapResults)
            {
                string sentanceStructure = item;
                if (addToBeginingOfNext.Length >= 1)
                {
                    sentanceStructure = (addToBeginingOfNext + item);
                }
                int findLastSpaceInMax = sentanceStructure.LastIndexOf(' ');
                var sentanceTruncated = sentanceStructure.Substring(0, findLastSpaceInMax);
                addToBeginingOfNext = sentanceStructure.Substring(findLastSpaceInMax, sentanceStructure.Length - findLastSpaceInMax);

                newWordWrap.Add(sentanceTruncated.Trim());
            }


            return newWordWrap;
        }
    }
}
