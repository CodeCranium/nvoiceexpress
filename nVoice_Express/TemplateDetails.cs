﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PdfSharp.Drawing;

namespace nVoice_Express
{
    class TemplateDetails
    {
        public enum TemplateColors
        {
            Blue = 0,
            Green = 1,
            Yellow = 2,
            Purple = 3,
            Red = 4,
            Orange = 5,
            CornflowerBlue = 6, 
            DarkGreen = 7, 
            DarkKhaki = 8, 
            DarkSlateGray = 9,
            DimGray = 10, 
            Lavender = 11, 
            Navy = 12, 
            Silver = 13, 
            Teal = 14, 
            Tan = 15, 
            Brown = 16
        }
        public static XBrush GetTemplateColor(TemplateColors templateColor)
        {
            XBrush tColor = null;
            switch (templateColor)
            {
                case TemplateColors.Blue : tColor = XBrushes.DarkBlue;
                    break;
                case TemplateColors.Green: tColor = XBrushes.Green;
                    break;
                case TemplateColors.Yellow: tColor = XBrushes.Yellow;
                    break;
                case TemplateColors.Purple: tColor = XBrushes.Purple;
                    break;
                case TemplateColors.Red: tColor = XBrushes.DarkRed;
                    break;
                case TemplateColors.Orange: tColor = XBrushes.Orange;
                    break;
                case TemplateColors.CornflowerBlue: tColor = XBrushes.CornflowerBlue;
                    break;
                case TemplateColors.DarkGreen: tColor = XBrushes.DarkGreen;
                    break;
                case TemplateColors.DarkKhaki: tColor = XBrushes.DarkKhaki;
                    break;
                case TemplateColors.DarkSlateGray: tColor = XBrushes.DarkSlateGray;
                    break;
                case TemplateColors.DimGray: tColor = XBrushes.DimGray;
                    break;
                case TemplateColors.Lavender: tColor = XBrushes.Lavender;
                    break;
                case TemplateColors.Navy: tColor = XBrushes.Navy;
                    break;
                case TemplateColors.Silver: tColor = XBrushes.Silver;
                    break;
                case TemplateColors.Teal: tColor = XBrushes.Teal;
                    break;
                case TemplateColors.Tan: tColor = XBrushes.Tan;
                    break;
                case TemplateColors.Brown: tColor = XBrushes.Brown;
                    break; 
            }

            return tColor;
        }

        public static IEnumerable<string> GetAvailableColors()
        {
            IList<string> colorList = new List<string>();
            foreach (var templateColor in Enum.GetValues(typeof(TemplateColors)))
            {
                colorList.Add(templateColor.ToString());
            }
            return colorList;
        }
    }
}
