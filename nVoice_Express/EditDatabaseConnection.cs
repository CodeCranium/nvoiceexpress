﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Windows.Forms;

namespace nVoice_Express
{
    public partial class EditDatabaseConnection : Form
    {
        nVoiceEntities nVoiceDB = new nVoiceEntities();

        public EditDatabaseConnection()
        {
            InitializeComponent();
        }

        private void EditDatabaseConnection_Load(object sender, EventArgs e)
        {
            RetrieveDatabaseSettings();
        }

        private void RetrieveDatabaseSettings()
        {
            txtbx_ConnectionString.Text = nVoiceDB.Database.Connection.ConnectionString;
            txtbx_SqlInstanceName.Text = nVoiceDB.Database.Connection.DataSource;
            txtbx_SqlDatabaseName.Text = nVoiceDB.Database.Connection.Database;
            txtbx_DatabaseConnectionProvider.Text = nVoiceDB.Database.Connection.ServerVersion;
        }

        private void SaveConnectionSettings(object sender, EventArgs e)
        {
            string configFileName = "nVoice_Express.exe.config";
            
            // Open config file.
            string text = File.ReadAllText(configFileName);

            // Replace values in config file.
            var ds = GetSettingInfo("data source", text);
            text = text.Replace(ds, txtbx_SqlInstanceName.Text);
            // text = text.Replace(GetSettingInfo("initial catalog", text), txtbx_SqlDatabaseName.Text);
            
            // Save config file.
            File.WriteAllText(configFileName, text);

            Application.Restart();
        }

        private string GetSettingInfo(string setting, string fullText)
        {
            string strStart = setting.ToLower() + "=";
            String strEnd = ";";
            int Start, End;
            Start = fullText.IndexOf(strStart, 0) + strStart.Length;
            End = fullText.IndexOf(strEnd, Start);
            return fullText.Substring(Start, End - Start);
        }

        private void OLDSaveConnectionSettings(object sender, EventArgs e)
        {
            // [origional connection string:]  
            //  <add name="nVoiceEntities" connectionString="metadata=res://*/Model_nVoiceDB.csdl|res://*/Model_nVoiceDB.ssdl|res://*/Model_nVoiceDB.msl;provider=System.Data.SqlClient;provider connection string=&quot;data source=localhost;initial catalog=nVoice;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework&quot;" providerName="System.Data.EntityClient" />

            var ds = "data source=" + txtbx_SqlInstanceName.Text;
            var ic = ";initial catalog=" + txtbx_SqlDatabaseName.Text;
            var sec = (chkbx_SQLTrustedSecurity.Checked) ? ";integrated security=True" : String.Format(";User Id={0};Password={1}", txtbx_Username.Text, txtbx_Password.Text);
            var mars = String.Format(";MultipleActiveResultSets=True;App=EntityFramework{0};", HttpUtility.HtmlEncode('"'));

            //var origionalConnectionString = "data source=localhost;initial catalog=nVoiceExpress;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework";
            //EntityConnectionStringBuilder ecsb = new EntityConnectionStringBuilder();
            //ecsb.ProviderConnectionString = ds + ic + sec + mars;

            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            
            ConnectionStringSettings conString = new ConnectionStringSettings()
            {
                Name = "nVoiceEntities",
                ConnectionString = String.Format(@"metadata=res://*/Model_nVoiceDB.csdl|res://*/Model_nVoiceDB.ssdl|res://*/Model_nVoiceDB.msl;provider=System.Data.SqlClient;provider connection string={0};", HttpUtility.HtmlEncode('"')) +
                ds + ic + sec + mars,
                ProviderName = "System.Data.EntityClient",
            };
            config.ConnectionStrings.ConnectionStrings.Clear();
            config.ConnectionStrings.ConnectionStrings.Add(conString);

            //config.AppSettings.Settings["username"].Value = m_strUserName;

            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("connectionStrings");
        }

        private void ShowHideSecUsernamePasswordOption(object sender, EventArgs e)
        {
            if (chkbx_SQLTrustedSecurity.Checked)
            {
                panel_UsernamePasswordSecurity.Visible = false;
            }
            else
            {
                panel_UsernamePasswordSecurity.Visible = true;
            }
        }

        private void btn_CreateNewDatabase_Click(object sender, EventArgs e)
        {
            // Present the option to run the Database Creation scripts 
            CreateInitialDatabase();
        }
        private void CreateInitialDatabase()
        {
            try
            {
                // First Time loading - Create database shell structure into SQL, and input nessicary data into it.
                nVoiceDB.Database.Create();  // Creates the database by name specified in the application.config
                Application.Restart();
            }
            catch (SqlException SqlException)
            {
                new ResultMessage() { ResultMessageInformation = "Unable to connect to the specified SQL instance. \n" + SqlException.Message, ResultType = ResultMessage.resultType.Info }.Alert();
            }
        }

        
    }
}
