﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace nVoice_Express
{
    public partial class AddCustomer : Form
    {
        public nVoiceEntities nVoiceDB = new nVoiceEntities();
        public AddCustomer()
        {
            InitializeComponent();
        }

        private void AddCustomerToDatabase(object sender, EventArgs e)
        {
            try
            {
                Client newClient = (new Client()
                                        {
                                            Firstname = txtbx_FirstName.Text,
                                            Lastname = txtbx_LastName.Text,
                                            Address = txtbx_StreetAddress.Text,
                                            City = txtbx_City.Text,
                                            State = txtbx_State.Text,
                                            Zipcode = txtbx_ZipCode.Text,
                                            EmailAddress = txtbx_EmailAddress.Text,
                                            PhoneNumber = txtbx_PhoneNumber.Text
                                        });

                var searchForDuplicates = nVoiceDB.Clients;
                //var duplicateFound = from x in searchForDuplicates.FirstOrDefault(x => (x.Firstname == newClient.Firstname && x.Lastname == newClient.Lastname Select(x);));
                Client duplicateFound = (from x in searchForDuplicates
                                     where x.Firstname == newClient.Firstname && x.Lastname == newClient.Lastname
                                     select x).FirstOrDefault();

                if (duplicateFound != null)
                {
                    var response_EditCurrentCustomer = MessageBox.Show("The customer information provided appears to already be in the system.  Do you want to CHANGE the details for the existing customer?", "Change current customer details", MessageBoxButtons.YesNoCancel);
                    
                    switch (response_EditCurrentCustomer)
                    {
                        case DialogResult.Cancel:
                            new ResultMessage(){ResultMessageInformation = "No changes have been made, and no customer data has been edited/saved.  Data not saved.", ResultType = ResultMessage.resultType.Error}.Alert();
                            break;
                        case DialogResult.No:
                            // Add another instance of the customer.
                            nVoiceDB.Clients.Add(newClient);
                            nVoiceDB.SaveChanges();
                                new ResultMessage(){ResultMessageInformation = "Customer has been added successfully.", ResultType = ResultMessage.resultType.Info}.Alert();
                            break;
                        case DialogResult.Yes:
                            // Edit the customers current details.
                           Client exisitingCustomer = nVoiceDB.Clients.Find(keyValues: duplicateFound.ClientID);
                               {
                                   exisitingCustomer.Firstname = newClient.Firstname;
                                   exisitingCustomer.Lastname = newClient.Lastname;

                                   exisitingCustomer.Address = newClient.Address;
                                   exisitingCustomer.City = newClient.City;
                                   exisitingCustomer.State = newClient.State;
                                   exisitingCustomer.Zipcode = newClient.Zipcode;

                                   exisitingCustomer.EmailAddress = newClient.EmailAddress;
                                   exisitingCustomer.PhoneNumber = newClient.PhoneNumber;
                               }
                           nVoiceDB.SaveChanges();
                                new ResultMessage() {ResultMessageInformation = "Customer has been updated successfully.", ResultType = ResultMessage.resultType.Info}.Alert();
                           break;
                    }
                }
                else
                {
                    // New instance of customer sent to DB
                    nVoiceDB.Clients.Add(newClient);
                    new ResultMessage() { ResultMessageInformation = "Customer has been updated successfully.", ResultType = ResultMessage.resultType.Info }.Alert();
                    nVoiceDB.SaveChanges();
                }

                this.Close();
                Application.Restart();  // Refresh Startup Form to get the newest results
            }
            catch (Exception ex)
            {
                new ResultMessage(){ResultMessageInformation = ex.Message, ResultType = ResultMessage.resultType.Error}.Alert();
            }
        }

        public void GetCustomerDetails(Client customerDetails)
        {
            try
            {
                txtbx_FirstName.Text = customerDetails.Firstname;
                txtbx_LastName.Text = customerDetails.Lastname;
                txtbx_StreetAddress.Text = customerDetails.Address;
                txtbx_City.Text = customerDetails.City;
                txtbx_State.Text = customerDetails.State;
                txtbx_ZipCode.Text = customerDetails.Zipcode;
                txtbx_EmailAddress.Text = customerDetails.EmailAddress;
                txtbx_PhoneNumber.Text = customerDetails.PhoneNumber;
            }
            catch (NullReferenceException noCustomerSelected)
            {
                new ResultMessage() { ResultMessageInformation = noCustomerSelected.Message, ResultType = ResultMessage.resultType.Error }.Alert();
            }
        }

        private void btn_DeleteCustomer_Click(object sender, EventArgs e)
        {
            var contactToDelete = (from x in nVoiceDB.Clients
                                  where x.Firstname == txtbx_FirstName.Text && x.Lastname == txtbx_LastName.Text 
                                  select x).FirstOrDefault();

            if (contactToDelete != null)
            {
                // Delete the provided Contact (from the First name and Last name identifier)
                nVoiceDB.Clients.Remove(contactToDelete);
                nVoiceDB.SaveChanges();
            }
            else
            {
                new ResultMessage() { ResultMessageInformation = "No customer First and Last name provided.  The First and Last name will need to be provided to identify which customer to DELETE.", ResultType = ResultMessage.resultType.Error }.Alert();
            }
            
        }

        private void CheckForExistingCustomer(object sender, EventArgs e)
        {
            foreach (var customerName in nVoiceDB.Clients)
            {
                if ((this.txtbx_FirstName.Text.ToLower() == customerName.Firstname.ToLower()) && (this.txtbx_LastName.Text.ToLower() == customerName.Lastname.ToLower()))
                {
                    btn_DeleteCustomer.Visible = true;
                    btn_AddCustomer.Text = "&Edit Customer";
                    break;
                }
                else
                {
                    btn_AddCustomer.Text = "&Add Customer";
                    btn_DeleteCustomer.Visible = false;
                }
            }
        }
    }
}
