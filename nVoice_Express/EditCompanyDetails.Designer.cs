﻿namespace nVoice_Express
{
    partial class EditCompanyDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_CompanyName = new System.Windows.Forms.Label();
            this.txtbx_CompanyName = new System.Windows.Forms.TextBox();
            this.txtbx_CompanyOwner = new System.Windows.Forms.TextBox();
            this.lbl_CompanyOwner = new System.Windows.Forms.Label();
            this.txtbx_CompanyCity = new System.Windows.Forms.TextBox();
            this.lbl_CompanyCity = new System.Windows.Forms.Label();
            this.txtbx_CompanyAddress = new System.Windows.Forms.TextBox();
            this.lbl_CompanyAddress = new System.Windows.Forms.Label();
            this.txtbx_CompanyEmailAddress = new System.Windows.Forms.TextBox();
            this.lbl_EmailAddress = new System.Windows.Forms.Label();
            this.txtbx_CompanyPhoneNumber = new System.Windows.Forms.TextBox();
            this.lbl_CompanyPhoneNumber = new System.Windows.Forms.Label();
            this.txtbx_CompanyZipCode = new System.Windows.Forms.TextBox();
            this.lbl_CompanyZipCode = new System.Windows.Forms.Label();
            this.txtbx_CompanyState = new System.Windows.Forms.TextBox();
            this.lbl_CompanyState = new System.Windows.Forms.Label();
            this.txtbx_CompanyWebsite = new System.Windows.Forms.TextBox();
            this.lbl_CompanyWebsite = new System.Windows.Forms.Label();
            this.btn_SaveCompanyDetails = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_CompanyName
            // 
            this.lbl_CompanyName.AutoSize = true;
            this.lbl_CompanyName.Location = new System.Drawing.Point(12, 12);
            this.lbl_CompanyName.Name = "lbl_CompanyName";
            this.lbl_CompanyName.Size = new System.Drawing.Size(82, 13);
            this.lbl_CompanyName.TabIndex = 0;
            this.lbl_CompanyName.Text = "Company Name";
            // 
            // txtbx_CompanyName
            // 
            this.txtbx_CompanyName.Location = new System.Drawing.Point(148, 9);
            this.txtbx_CompanyName.Name = "txtbx_CompanyName";
            this.txtbx_CompanyName.Size = new System.Drawing.Size(231, 20);
            this.txtbx_CompanyName.TabIndex = 1;
            // 
            // txtbx_CompanyOwner
            // 
            this.txtbx_CompanyOwner.Location = new System.Drawing.Point(148, 31);
            this.txtbx_CompanyOwner.Name = "txtbx_CompanyOwner";
            this.txtbx_CompanyOwner.Size = new System.Drawing.Size(231, 20);
            this.txtbx_CompanyOwner.TabIndex = 3;
            // 
            // lbl_CompanyOwner
            // 
            this.lbl_CompanyOwner.AutoSize = true;
            this.lbl_CompanyOwner.Location = new System.Drawing.Point(12, 34);
            this.lbl_CompanyOwner.Name = "lbl_CompanyOwner";
            this.lbl_CompanyOwner.Size = new System.Drawing.Size(85, 13);
            this.lbl_CompanyOwner.TabIndex = 2;
            this.lbl_CompanyOwner.Text = "Company Owner";
            // 
            // txtbx_CompanyCity
            // 
            this.txtbx_CompanyCity.Location = new System.Drawing.Point(148, 75);
            this.txtbx_CompanyCity.Name = "txtbx_CompanyCity";
            this.txtbx_CompanyCity.Size = new System.Drawing.Size(231, 20);
            this.txtbx_CompanyCity.TabIndex = 7;
            // 
            // lbl_CompanyCity
            // 
            this.lbl_CompanyCity.AutoSize = true;
            this.lbl_CompanyCity.Location = new System.Drawing.Point(12, 78);
            this.lbl_CompanyCity.Name = "lbl_CompanyCity";
            this.lbl_CompanyCity.Size = new System.Drawing.Size(71, 13);
            this.lbl_CompanyCity.TabIndex = 6;
            this.lbl_CompanyCity.Text = "Company City";
            // 
            // txtbx_CompanyAddress
            // 
            this.txtbx_CompanyAddress.Location = new System.Drawing.Point(148, 53);
            this.txtbx_CompanyAddress.Name = "txtbx_CompanyAddress";
            this.txtbx_CompanyAddress.Size = new System.Drawing.Size(231, 20);
            this.txtbx_CompanyAddress.TabIndex = 5;
            // 
            // lbl_CompanyAddress
            // 
            this.lbl_CompanyAddress.AutoSize = true;
            this.lbl_CompanyAddress.Location = new System.Drawing.Point(12, 56);
            this.lbl_CompanyAddress.Name = "lbl_CompanyAddress";
            this.lbl_CompanyAddress.Size = new System.Drawing.Size(92, 13);
            this.lbl_CompanyAddress.TabIndex = 4;
            this.lbl_CompanyAddress.Text = "Company Address";
            // 
            // txtbx_CompanyEmailAddress
            // 
            this.txtbx_CompanyEmailAddress.Location = new System.Drawing.Point(148, 163);
            this.txtbx_CompanyEmailAddress.Name = "txtbx_CompanyEmailAddress";
            this.txtbx_CompanyEmailAddress.Size = new System.Drawing.Size(231, 20);
            this.txtbx_CompanyEmailAddress.TabIndex = 15;
            // 
            // lbl_EmailAddress
            // 
            this.lbl_EmailAddress.AutoSize = true;
            this.lbl_EmailAddress.Location = new System.Drawing.Point(12, 166);
            this.lbl_EmailAddress.Name = "lbl_EmailAddress";
            this.lbl_EmailAddress.Size = new System.Drawing.Size(120, 13);
            this.lbl_EmailAddress.TabIndex = 14;
            this.lbl_EmailAddress.Text = "Company Email Address";
            // 
            // txtbx_CompanyPhoneNumber
            // 
            this.txtbx_CompanyPhoneNumber.Location = new System.Drawing.Point(148, 141);
            this.txtbx_CompanyPhoneNumber.Name = "txtbx_CompanyPhoneNumber";
            this.txtbx_CompanyPhoneNumber.Size = new System.Drawing.Size(231, 20);
            this.txtbx_CompanyPhoneNumber.TabIndex = 13;
            // 
            // lbl_CompanyPhoneNumber
            // 
            this.lbl_CompanyPhoneNumber.AutoSize = true;
            this.lbl_CompanyPhoneNumber.Location = new System.Drawing.Point(12, 144);
            this.lbl_CompanyPhoneNumber.Name = "lbl_CompanyPhoneNumber";
            this.lbl_CompanyPhoneNumber.Size = new System.Drawing.Size(125, 13);
            this.lbl_CompanyPhoneNumber.TabIndex = 12;
            this.lbl_CompanyPhoneNumber.Text = "Company Phone Number";
            // 
            // txtbx_CompanyZipCode
            // 
            this.txtbx_CompanyZipCode.Location = new System.Drawing.Point(148, 119);
            this.txtbx_CompanyZipCode.Name = "txtbx_CompanyZipCode";
            this.txtbx_CompanyZipCode.Size = new System.Drawing.Size(231, 20);
            this.txtbx_CompanyZipCode.TabIndex = 11;
            // 
            // lbl_CompanyZipCode
            // 
            this.lbl_CompanyZipCode.AutoSize = true;
            this.lbl_CompanyZipCode.Location = new System.Drawing.Point(12, 122);
            this.lbl_CompanyZipCode.Name = "lbl_CompanyZipCode";
            this.lbl_CompanyZipCode.Size = new System.Drawing.Size(97, 13);
            this.lbl_CompanyZipCode.TabIndex = 10;
            this.lbl_CompanyZipCode.Text = "Company Zip Code";
            // 
            // txtbx_CompanyState
            // 
            this.txtbx_CompanyState.Location = new System.Drawing.Point(148, 97);
            this.txtbx_CompanyState.Name = "txtbx_CompanyState";
            this.txtbx_CompanyState.Size = new System.Drawing.Size(231, 20);
            this.txtbx_CompanyState.TabIndex = 9;
            // 
            // lbl_CompanyState
            // 
            this.lbl_CompanyState.AutoSize = true;
            this.lbl_CompanyState.Location = new System.Drawing.Point(12, 100);
            this.lbl_CompanyState.Name = "lbl_CompanyState";
            this.lbl_CompanyState.Size = new System.Drawing.Size(79, 13);
            this.lbl_CompanyState.TabIndex = 8;
            this.lbl_CompanyState.Text = "Company State";
            // 
            // txtbx_CompanyWebsite
            // 
            this.txtbx_CompanyWebsite.Location = new System.Drawing.Point(148, 185);
            this.txtbx_CompanyWebsite.Name = "txtbx_CompanyWebsite";
            this.txtbx_CompanyWebsite.Size = new System.Drawing.Size(231, 20);
            this.txtbx_CompanyWebsite.TabIndex = 17;
            // 
            // lbl_CompanyWebsite
            // 
            this.lbl_CompanyWebsite.AutoSize = true;
            this.lbl_CompanyWebsite.Location = new System.Drawing.Point(12, 188);
            this.lbl_CompanyWebsite.Name = "lbl_CompanyWebsite";
            this.lbl_CompanyWebsite.Size = new System.Drawing.Size(93, 13);
            this.lbl_CompanyWebsite.TabIndex = 16;
            this.lbl_CompanyWebsite.Text = "Company Website";
            // 
            // btn_SaveCompanyDetails
            // 
            this.btn_SaveCompanyDetails.Location = new System.Drawing.Point(242, 234);
            this.btn_SaveCompanyDetails.Name = "btn_SaveCompanyDetails";
            this.btn_SaveCompanyDetails.Size = new System.Drawing.Size(137, 23);
            this.btn_SaveCompanyDetails.TabIndex = 18;
            this.btn_SaveCompanyDetails.Text = "Save Company Details";
            this.btn_SaveCompanyDetails.UseVisualStyleBackColor = true;
            this.btn_SaveCompanyDetails.Click += new System.EventHandler(this.SaveCompanyDetails);
            // 
            // EditCompanyDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 269);
            this.Controls.Add(this.btn_SaveCompanyDetails);
            this.Controls.Add(this.txtbx_CompanyWebsite);
            this.Controls.Add(this.lbl_CompanyWebsite);
            this.Controls.Add(this.txtbx_CompanyEmailAddress);
            this.Controls.Add(this.lbl_EmailAddress);
            this.Controls.Add(this.txtbx_CompanyPhoneNumber);
            this.Controls.Add(this.lbl_CompanyPhoneNumber);
            this.Controls.Add(this.txtbx_CompanyZipCode);
            this.Controls.Add(this.lbl_CompanyZipCode);
            this.Controls.Add(this.txtbx_CompanyState);
            this.Controls.Add(this.lbl_CompanyState);
            this.Controls.Add(this.txtbx_CompanyCity);
            this.Controls.Add(this.lbl_CompanyCity);
            this.Controls.Add(this.txtbx_CompanyAddress);
            this.Controls.Add(this.lbl_CompanyAddress);
            this.Controls.Add(this.txtbx_CompanyOwner);
            this.Controls.Add(this.lbl_CompanyOwner);
            this.Controls.Add(this.txtbx_CompanyName);
            this.Controls.Add(this.lbl_CompanyName);
            this.Name = "EditCompanyDetails";
            this.Text = "EditCompanyDetails";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_CompanyName;
        private System.Windows.Forms.TextBox txtbx_CompanyName;
        private System.Windows.Forms.TextBox txtbx_CompanyOwner;
        private System.Windows.Forms.Label lbl_CompanyOwner;
        private System.Windows.Forms.TextBox txtbx_CompanyCity;
        private System.Windows.Forms.Label lbl_CompanyCity;
        private System.Windows.Forms.TextBox txtbx_CompanyAddress;
        private System.Windows.Forms.Label lbl_CompanyAddress;
        private System.Windows.Forms.TextBox txtbx_CompanyEmailAddress;
        private System.Windows.Forms.Label lbl_EmailAddress;
        private System.Windows.Forms.TextBox txtbx_CompanyPhoneNumber;
        private System.Windows.Forms.Label lbl_CompanyPhoneNumber;
        private System.Windows.Forms.TextBox txtbx_CompanyZipCode;
        private System.Windows.Forms.Label lbl_CompanyZipCode;
        private System.Windows.Forms.TextBox txtbx_CompanyState;
        private System.Windows.Forms.Label lbl_CompanyState;
        private System.Windows.Forms.TextBox txtbx_CompanyWebsite;
        private System.Windows.Forms.Label lbl_CompanyWebsite;
        private System.Windows.Forms.Button btn_SaveCompanyDetails;
    }
}