//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace nVoice_Express
{
    using System;
    using System.Collections.Generic;
    
    public partial class ServiceInvoice
    {
        public int InvoiceID { get; set; }
        public int ClientID { get; set; }
        public string ServiceID { get; set; }
        public string ServicePrice { get; set; }
        public Nullable<bool> Unpaid { get; set; }
        public Nullable<System.DateTime> DateOfService { get; set; }
    }
}
