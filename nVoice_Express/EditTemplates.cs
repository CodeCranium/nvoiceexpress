﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace nVoice_Express
{
    public partial class EditTemplates : Form
    {
        nVoiceEntities nVoiceDB = new nVoiceEntities();

        public EditTemplates()
        {
            InitializeComponent();
            PopulateColorsForList();
            PopulateFontsForList();
        }

        private void PopulateFontsForList()
        {
            InstalledFontCollection fontsCollection = new InstalledFontCollection();

            foreach (var font in fontsCollection.Families)
            {
                dropdown_FontsList.Items.Add(font.Name);
            }
        }

        private void PopulateColorsForList()
        {
            foreach (var color in TemplateDetails.GetAvailableColors())
            {
                dropdown_ColorList.Items.Add(color);
            }
            
        }

        private void SaveUpdateTemplate(object sender, EventArgs e)
        {
            InvoiceTemplate newTemplate = new InvoiceTemplate()
                                              {
                                                  TemplateName = txtbx_TemplateName.Text,
                                                  Header = txtbx_TemplateHeader.Text,
                                                  Footer = txtbx_TemplateFooter.Text,
                                                  Disclamer = txtbx_TemplateDisclamer.Text,
                                                  SchemeColor = dropdown_ColorList.SelectedItem.ToString(),
                                                  FontName = dropdown_FontsList.SelectedItem.ToString(),
                                                  UseCompanyLogoPhoto = (bool) GetCheckBoxValue(chkbx_UsePhotoForCompanyLogo),
                                                  ShowBarCode = (bool) GetCheckBoxValue(chkbx_ShowBarCode)
                                              };
            var findDuplicateTemplateName = (from x in nVoiceDB.InvoiceTemplates
                                            where x.TemplateName == newTemplate.TemplateName
                                            select x).FirstOrDefault();

            if (findDuplicateTemplateName != null)
            {
                findDuplicateTemplateName.Header = txtbx_TemplateHeader.Text;
                findDuplicateTemplateName.Footer = txtbx_TemplateFooter.Text;
                findDuplicateTemplateName.Disclamer = txtbx_TemplateDisclamer.Text;
                findDuplicateTemplateName.SchemeColor = dropdown_ColorList.SelectedItem.ToString();
                findDuplicateTemplateName.FontName = dropdown_FontsList.SelectedItem.ToString();
            }
            else
            {
                nVoiceDB.InvoiceTemplates.Add(newTemplate);
            }

            nVoiceDB.SaveChanges();
            this.Close();

            Application.Restart();  // Refresh Startup Form to get the newest results
        }


        private bool? GetCheckBoxValue(CheckBox chkbxName)
        {
            if (chkbxName.Checked)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void btn_DeleteTemplate_Click(object sender, EventArgs e)
        {
            var tempalteToDelete = (from x in nVoiceDB.InvoiceTemplates
                                   where x.TemplateName == txtbx_TemplateName.Text
                                   select x).FirstOrDefault();

            if (tempalteToDelete != null)
            {
                nVoiceDB.InvoiceTemplates.Remove(tempalteToDelete);
                nVoiceDB.SaveChanges();
            }
            else
            {
                new ResultMessage() { ResultMessageInformation = "No Template name, or incorrect Template name provided.  The Template name will need to be provided to identify which template to DELETE.", ResultType = ResultMessage.resultType.Error }.Alert();
                txtbx_TemplateName.Focus();
            }
            
        }

        public void GetTemplateDetails(InvoiceTemplate templateChosen)
        {
            txtbx_TemplateName.Text = templateChosen.TemplateName;
            txtbx_TemplateHeader.Text = templateChosen.Header;
            txtbx_TemplateFooter.Text = templateChosen.Footer;
            txtbx_TemplateDisclamer.Text = templateChosen.Disclamer;
            dropdown_ColorList.SelectedItem = templateChosen.SchemeColor.ToString();
            dropdown_FontsList.SelectedItem = templateChosen.FontName.ToString();

            chkbx_UsePhotoForCompanyLogo.Checked = templateChosen.UseCompanyLogoPhoto;
            chkbx_ShowBarCode.Checked = templateChosen.ShowBarCode;
        }

        private void CheckForMatchingTemplateName(object sender, EventArgs e)
        {
            foreach (var templateName in nVoiceDB.InvoiceTemplates)
            {
                if (this.txtbx_TemplateName.Text.ToLower() == templateName.TemplateName.ToLower())
                {
                    btn_DeleteTemplate.Visible = true;
                    btn_SaveTemplate.Text = "&Edit Template";
                    break;
                }
                else
                {
                    btn_DeleteTemplate.Visible = false;
                    btn_SaveTemplate.Text = "&Add Template";
                }
            }
        }
    }
}
